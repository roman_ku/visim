<?php
require_once(APPPATH.'libraries/Curl_func.php');


class Retail_Lib extends Curl_Func
{
	
	function __construct()
    {
        parent::__construct();




       // $this->load->library('prom_lib');

    }

    public function create_order($order_to_crm, $site) {

        $client = new \RetailCrm\ApiClient(
            'https://wordpress-visim.retailcrm.ru',
            '3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g',
            \RetailCrm\ApiClient::V5
        );

        try {

        	$res = $client->request->ordersCreate($order_to_crm, $site);

			return $res;

        } catch (\RetailCrm\Exception\CurlException $e) {
            echo "Connection error: " . $e->getMessage();
        }


    }

    public function getHistory($time_from) {
        $headers = array('Access-Control-Allow-Origin: *','Content-Type: application/json');

        $history_response = $this->doCurl("https://wordpress-visim.retailcrm.ru/api/v5/orders/history?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g&&limit=100&&filter[startDate]=" . $time_from, $headers, 'GET');
        $historys = json_decode($history_response);
        $historys = json_decode(json_encode($historys), True);

        return $historys;
    }

    public function updateStatus($field_status, $history_change, $id){
		$CI =& get_instance();

		$CI->load->library('prom_lib');

        $order_crm = $this->getOrderById($id);
        if($order_crm['order']['orderMethod'] == 'prom-visim'){

            $external_id = $order_crm['order']['externalId'];

            if($field_status == 'status' && $history_change['newValue']['code'] != 'offer-analog'){  

                //echo '<pre>';var_dump($history_change);die();

                $status = $CI->prom_lib->getStatusForProm($history_change['newValue']['code']);

                if(isset($status)) {
					if($status == 'canceled'){

						$cancellationReason = $CI->prom_lib->getCancellationReason($history_change['newValue']['code']);

						if($cancellationReason == 'price_changed') {
							$reasonText = 'Ціна змінена';
						}
						if(isset($reasonText)) {
							$CI->prom_lib->setStatus($external_id, $status, $cancellationReason, $reasonText); // post data to prom
						} else {
							$CI->prom_lib->setStatus($external_id, $status, $cancellationReason); // post data to prom
						}

					} else {
						$CI->prom_lib->setStatus($external_id, $status); // post data to prom
					}
                }

            }
            //else if($field_status == 'integration_delivery_data.track_number' /*&& $history_change['order']['site'] == $site*/){   // add delivery track_number
               
                //$track_number = $history_change['newValue'];

                //$response = $this->change_order_on_rozetka($external_id, $info['prom_ua_key'], 3, $track_number); // post data to rozetka
            
                //if($response ['success'] == false) {
                    //$this->change_order_on_rozetka($external_id, $info['sh_key'], 3, $track_number); // post data to rozetka
                //}

                //if(isset($order_crm['order']['customFields']['moyskladexternalid'])){
                    //$this->moysklad_retail->update_moysklad_ttn($order_crm['order']['customFields']['moyskladexternalid'], $track_number);
                //}

            //}
            //else if($field_status == 'order_product' /*&& $history_change['order']['site'] == $site*/) {  // add product to order
                /*$item_id = $history_change['item']['id'];

                $order_info = $this->get_order_from_crm_by_id($id);   // get info about order by id
               
                foreach ($order_info['order']['items'] as $item) {
                    if ($item_id == $item['id']) {
                        $purchases = array(array(
                            'id' => null,
                            'cost' => $item['initialPrice'] * $item['quantity'],
                            'price' => $item['initialPrice'],
                            'price_with_discount' => $item['initialPrice'],
                            'quantity' => $item['quantity'],
                            'item_id' => $item['offer']['externalId'],
                            'item_name' => $item['offer']['name'],
                            'kit_id' => null
                        ));
                    }

                }
                $new_item = array(
                    'amount' => $order_info['order']['summ'],
                    'amount_with_discount' => $order_info['order']['totalSumm'],
                    'purchases' => $purchases
                );

                $this->add_product_to_order($info['prom_ua_key'], $external_id, $new_item);
                $this->add_product_to_order($info['sh_key'], $external_id, $new_item);*/

            //}
            //else if($field_status == 'order_product.quantity' /*&& $history_change['order']['site'] == $site*/) {
               
                /*$item_id = $history_change['item']['id'];

                $order_info = $this->get_order_from_crm_by_id($id);   // get info about order by id

                $order_info_rozetka = $this->order_info_rozetka_by_id(intval($external_id), $info['prom_ua_key']);
                if($order_info_rozetka ['success'] == false) {
                    $order_info_rozetka = $this->order_info_rozetka_by_id(intval($external_id), $info['sh_key']);
                }
                foreach ($order_info['order']['items'] as $item) {

                    if ($item_id == $item['id']) {
                        $index = array_search($item,$order_info['order']['items']);
                        $purchases =  array(array(
                            'id' => $order_info_rozetka['content']['purchases'][$index]['id'],
                            'quantity' => $item['quantity'],
                            'cost' => $item['quantity'] * $order_info['order']['items'][$index]['initialPrice'],
                            'cost_with_discount' => $item['quantity'] * ($order_info['order']['items'][$index]['initialPrice'] - $order_info['order']['items'][$index]['discountTotal'])
                        ));
                    }

                }
                $order = array("amount" => $order_info['order']['summ'], "amount_with_discount" => $order_info['order']['totalSumm'], "purchases" => $purchases);

                $res = $this->add_product_to_order($info['prom_ua_key'], $external_id, $order);
                if($res ['success'] == false) {
                    $this->add_product_to_order($info['sh_key'], $external_id, $order);
                }
            //}
            else if($field_status == 'custom_moyskladexternalid') {

            }*/
          
        }
    }

    

    public function getOrderById($id){

        $url = "https://wordpress-visim.retailcrm.ru/api/v5/orders/" . $id ."?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g&&by=id";

        return $this->apiRequest($url, 'GET');

    }

    function apiRequest($url, $method) {
    
        $data  = $this->doCurl($url, false, $method);

        $data = json_decode($data);
        $data = json_decode(json_encode($data), True);

        return $data;

    }
}


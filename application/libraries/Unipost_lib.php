<?php
require_once(APPPATH.'libraries/Curl_func.php');


class Unipost_Lib extends Curl_Func
{

	const TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2MDAyNzA0MTIsImV4cCI6MTYzMTE0OTQxMiwicm9sZXMiOlsiUk9MRV9VU0VSIiwiUk9MRV9DTElFTlQiXSwidXNlcm5hbWUiOiJuZXN0ZXJlbmtvQHZpc2ltLmJpeiJ9.P8WQ4kwvyaEEGEcKSRzhwcBHt4fz3akUF8J5OPyqZ1B7bJcPm0dnTJUhA_Cp8FRxykGnC67mFqH1APLBaM6upGmSH8P4BjIjf6V7-KbsknSNrt-7Sv6rf-fGZXd3Ppn9CPpTi1an_csF3InImOrD0pNAZmhcwj3KRd4nuRJw0taMvvMo7AoNzVQIB-ELDanFPCEuwIeXgyBL8em3FcNQw5qtzacrWn_EI36Y9KjeDD88boehm8m0ckajqjcgmo4ymSJM3CnndkBl5-3cLJLGSpK4K8OQkryCTD5ZApNZ57bly257lnC8tVTOevwzkuQfw0Nn7VHoZYvs07FlqfaEDOEVt8iDZPALzCGS9IGxNddkqzOY8k-b2oQYjpNA-w02ReDZ4gPi0oTViCqc6s3u-bwr9yYLRUu0MzG_ohmBRBsCckkNa55GX0XnQQ2-TRtKFO84_x5nBCcXmUay3_obcMkr5Vqw0ekgQHzkeLoEUvzJ3TBPcOayZjIytobDrzgK_cjsVCE1O_pcP1OtrL8zierMjBaCSQ0AHfSWrL8IBzzeM2tgASx9NTolOmqiGoUj92aY7M7--DG_DxB0eFJZ0jbc_SVN9IJaggxCLu5Eh4i2TSEcoyG4APuDJR4-cMezU-MK107q-Z1gBTSghZPL7YsHLIFDMXX03NoamfV22XI";

	private $header;

	function __construct()
	{
		parent::__construct();

		require FCPATH . 'vendor/autoload.php';

		$this->header = array('Authorization: Bearer ' . self::TOKEN, 'Content-Type: application/json');

	}

	public function createPostavka($products){
		$url = "http://my.unipost.com.ua/api/v1/shipment/create";

		$post_arr = array(
			"comment"    =>  $products['description'],
			"shipmentProducts" => $products['item']
		);

		$response = $this->doCurl($url, $this->header, 'POST', json_encode($post_arr));

	}

	public function prepareDataForPackage($order, $postOffice, $city)
	{
		($order['delivery']['data']['payerType'] == 'Recipient')? $whoIsPaid = 'recipient': $whoIsPaid = 'sender';
		(isset($order['delivery']['address']['street']))? $street = $order['delivery']['address']['street'] : $street = '';
		(isset($order['delivery']['address']['building']))? $building = $order['delivery']['address']['building'] : $building = '';
		(isset($order['delivery']['address']['flat']))? $flat = $order['delivery']['address']['flat'] : $flat = '';

		foreach ($order['payments'] as $payment) {
			($payment['status'] == 'paid')? $cod = 0: $cod = $payment['amount'];
		}
		
		$products = array();
		foreach ($order['items'] as $item) {
			$products[] = array(
				"product" 			=> $item['offer']['article'],
				"awaitingCount"		=> strval($item['quantity'])
			);

		}
		(floatval($order['delivery']['data']['declaredValue']) < 200 )?$declaredCost= floatval(200): floatval($order['delivery']['data']['declaredValue']);
		if(isset($order['address'])) {
			$shippingMethodId = 2;
		} else {
			$shippingMethodId = 5;
		}

		$post_arr = array(
			'externalId' 		=> $order['number'],
			'weight'     		=> $order['weight'] / 1000,
			'declaredCost'      => $declaredCost,
			'cod'               => floatval($cod),
			'officeId'        	=> $postOffice['post_office_id'],
			'shippingMethodId'	=> $shippingMethodId,
			'city'              => $city,
			'street'			=> $street.' '.$building,
			'apartment'			=> floatval($flat),
			'firstName'			=> $order['contact']['firstName'],
			'lastName'			=> $order['contact']['lastName'],
			'phone'				=> $order['contact']['phones'][0]['number'],
			'email'				=> $order['contact']['email'],
			'whoIsPaid'			=> $whoIsPaid,
			'products'			=> $products
		);
		var_dump($post_arr);

		return $post_arr;

	}

	public function createPackage($post_arr){
		$url = "https://my.unipost.com.ua/api/v2/package/create";

		$response = $this->doCurl($url, $this->header, 'POST', json_encode($post_arr));
		echo '<pre>';var_dump(json_decode($response));die();
	}

	public function getCityList($page = 1){
		$url = "http://my.unipost.com.ua/api/v2/directory/city/list?page=".$page;

		$response = $this->doCurl($url, $this->header, 'GET');
		return json_decode($response);
	}

	public function getPostOffices($page = 1){
		$url = "http://my.unipost.com.ua/api/v2/directory/post-office/list?page=".$page;

		$response = $this->doCurl($url, $this->header, 'GET');

		return json_decode($response);
	}


}

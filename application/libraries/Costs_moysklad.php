<?php
require_once(APPPATH.'libraries/Curl_func.php');


class Costs_moysklad extends Curl_Func
{

    private $moysklad_login; // moysklad login;
    private $moysklad_password;    // moysklad password

    function __construct()
    {

        $this->moysklad_login = 'kushka@visim';
        $this->moysklad_password = '6hBBNyDB3xsVH42';
    
    }

    private function header(){

        $code = base64_encode($this->moysklad_login.':'.$this->moysklad_password);

        return array(
            "authorization: Basic ". $code,
            "content-type: application/json"
        );
    }

    private function moysclad_request($url, $method, $post_arr = null , $header = null){
        
        if(isset($header)){
            $response =  $this->doCurl($url,$header,$method, $post_arr);
        } else {
            $post_arr['moysklad_login'] = array(
                    'name' => $this->moysklad_login,
                    'password' => $this->moysklad_password
             );     

            $response =  $this->doCurl($url,'',$method, $post_arr);
        }

        $response = json_decode($response);
        $response = json_decode(json_encode($response), True);

        return $response;
        
    }

    public function getCosts($url, $method) {

        $response = $this->moysclad_request($url, $method);

        return $response;
    }


    public function setCosts($url, $method, $post_arr) {

        $header = $this->header();

        $response = $this->moysclad_request($url, $method, json_encode($post_arr), $header);

        return $response;
    }
}

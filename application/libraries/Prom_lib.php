<?php
require_once(APPPATH.'libraries/Curl_func.php');


class Prom_Lib extends Curl_Func
{
	
	function __construct()
    {
        parent::__construct();

    }

    private function promRequest($url, $method, $data = null){

    	$token = '512cefe7a03467c995b114687b0c44a513258ca4'; // prom token;

        $headers = array('Authorization: Bearer ' . $token, 'Content-Type: application/json');

        if(isset($data)) {
			return json_decode($this->doCurl($url, $headers, $method, $data), true);
		} else {
			return json_decode($this->doCurl($url, $headers, $method), true);
		}

    }

    public function getNewOrder($status) {

        $url = 'http://my.prom.ua/api/v1/orders/list?limit=100status='.$status;

        return $this->promRequest($url, 'GET');
    }


    public function getStatusForProm($data){
        switch($data) {

            case 'send-to-assembling':
                $status = 'received';
                return $status;
                break;
            case 'client-confirmed':
                $status = 'received';
                return $status;
                break;
            case 'no-call':
                $status = 'canceled';
                return $status;
                break;
            case 'no-product':
                $status = 'canceled';
                return $status;
                break;
            case 'already-buyed':
                $status = 'canceled';
                return $status;
                break;
            case 'delyvery-did-not-suit':
                $status = 'canceled';
                return $status;
                break;
            case 'prices-did-not-suit':
                $status = 'canceled';
                return $status;
                break;
            case 'cancel-other':
                $status = 'canceled';
                return $status;
                break;
            case 'proc':
                $status = 'received';
                return $status;
                break;
            case 'op':
                $status = 'received';
                return $status;
                break;
            case 'offer-analog':
                $status = 26;
                return $status;
                break;
            case 'nvrz':
                $status = 'canceled';
                return $status;
                break;
            case 'nvso':
                $status = 'canceled';
                return $status;
                break;
            case 'omn1':
                $status = 'received';
                return $status;
                break;
            case 'omn2':
                $status = 'received';
                return $status;
                break;
            case 'mark':
                $status = 'received';
                return $status;
                break;
            case 'vidmk':
                $status = 'canceled';
                return $status;
                break;
            case 'nnvn':
                $status = 'received';
                return $status;
                break;
            case 'potpidnay':
                $status = 'received';
                return $status;
                break;
            case 'availability-confirmed':
                $status = 'received';
                return $status;
                break;
            case 'pnpo':
                $status = 'received';
                return $status;
                break;
            case 'vtpp':
                $status = 'received';
                return $status;
                break;
            case 'srpp':
                $status = 'received';
                return $status;
                break;
            case 'chtpp':
                $status = 'received';
                return $status;
                break;
            case 'ptpp':
                $status = 'received';
                return $status;
                break;
            case 'sbpp':
                $status = 'received';
                return $status;
                break;
            case 'ndpp':
                $status = 'received';
                return $status;
                break;
            case 'nettn':
                $status = 'received';
                return $status;
                break;
            case 'prepayed':
                $status = 'paid';
                return $status;
                break;
            case 'sppp':
                $status = 'received';
                return $status;
                break;
            case 'oppnn':
                $status = 'received';
                return $status;
                break;
			case 'zm':
				$status = 'received';
				return $status;
				break;
			case 'zo':
				$status = 'received';
				return $status;
				break;
			case 'pv':
				$status = 'received';
				return $status;
				break;
			case 'po':
				$status = 'received';
				return $status;
				break;
			case 'kpnk':
				$status = 'received';
				return $status;
				break;
			case 'knopk':
				$status = 'received';
				return $status;
				break;
			case 'db':
				$status = 'received';
				return $status;
				break;
			case 'up1':
				$status = 'received';
				return $status;
				break;
			case 'up2':
				$status = 'received';
				return $status;
				break;
			case 'up3':
				$status = 'received';
				return $status;
				break;
			case 'up4':
				$status = 'received';
				return $status;
				break;
			case 'up5':
				$status = 'received';
				return $status;
				break;
			case 'vpdv':
				$status = 'received';
				return $status;
				break;
			case 'psd':
				$status = 'received';
				return $status;
				break;
			case 'delivering':
				$status = 'received';
				return $status;
				break;
			case 'redirect':
				$status = 'received';
				return $status;
				break;
			case 'otrymano':
				$status = 'delivered';
				return $status;
				break;
			case 'kot':
				$status = 'delivered';
				return $status;
				break;
			case 'complete':
				$status = 'delivered';
				return $status;
				break;
			case 'vp':
				$status = 'delivered';
				return $status;
				break;
			case 'zmv':
				$status = 'delivered';
				return $status;
				break;
			case 'pvv':
				$status = 'delivered';
				return $status;
				break;
			case 'mmzv':
				$status = 'delivered';
				return $status;
				break;
			case 'vvo':
				$status = 'canceled';
				return $status;
				break;
			case 'vvoz':
				$status = 'canceled';
				return $status;
				break;
			case 'kp':
				$status = 'canceled';
				return $status;
				break;
			case 'duble':
				$status = 'canceled';
				return $status;
				break;
			case 'less_then_minimal_price':
				$status = 'canceled';
				return $status;
				break;
        }
    }

    public function setStatus($external_id, $status, $cancellationReason = false, $reasonText = false){
		$url = 'https://my.prom.ua/api/v1/orders/set_status';

		if($cancellationReason && !$reasonText) {
			$data = '{
				"status": "'.$status.'",
				"ids": ['.$external_id.'],
				"cancellation_reason": "' .$cancellationReason.'"
			}';
		} elseif ($reasonText && $cancellationReason){
			$data = '{
				"status": "'.$status.'",
				"ids": ['.$external_id.'],
				"cancellation_reason": "' .$cancellationReason.'",
				"cancellation_text": "'.$reasonText.'"
				}';
		}
		else {
			$data = '{
				"status": "'.$status.'",
				"ids": ['.$external_id.']
			}';
		}

		$response =  $this->promRequest($url, 'POST', $data);

		return $response;
    }

    public function editProduct($products){
		$i=0;
		$url = 'https://my.prom.ua/api/v1/products/edit_by_external_id';
		$data = '[';

		foreach ($products as $product) {
			$saleValue = null;
			$discountStart = null;
			$discountEnd = null;
			$priceValue = null;
			$status = null;

			if(isset($product['salePrices'])) {
				foreach ($product['salePrices'] as $price) {
					if($price['priceType'] == 'Ціна PROM' && $price['value'] != 0) {
						$priceValue = $price['value'] / 100;
					} else {
						$priceValue = false;
					}
				}
			}

			if(isset($product['attributes'])){
				foreach ($product['attributes'] as  $item) {
					if($item['name'] == "PROM старт знижки"){
						$discountStart = strtotime($item['value']);
						$discountStart = date('d.m.Y',$discountStart);
					}

					if($item['name'] == "PROM кінець знижки"){
						$discountEnd = strtotime($item['value']);
						$discountEnd = date('d.m.Y', $discountEnd);
					}

					if($item['name'] == "PROM знижка") {
						$saleValue = $item['value'];
					}

					if($item['name'] == "Статус Prom"){
						$status = $this->getProductStatus($item['value']['name']);
					}

				}


			}

			$data = $data .'{"id": "' . intval($product['externalCode']) . '"'.
				((isset($saleValue) && isset($discountStart) && isset($discountEnd))?',"discount": {"value": "' . number_format($saleValue, 0, ".", "") . '","type": "amount","date_start": "' . $discountStart . '","date_end": "' . $discountEnd . '"}':"").
				(isset($priceValue)?',"price": ' . number_format($priceValue, 2, ".", ""): "").
				(isset($status)?',"presence": "' . $status["available"] . '","quantity_in_stock": ' . $status["stock_quantity"] : "") .
				'},';

			if($i==99){
				$i=0;
				$data = rtrim($data, ",");
				$data = $data.']';
				$res = $this->promRequest($url, 'POST', $data);
				$data = '[';
				echo '<pre>';var_dump( $res);
			} else {
				$i++;
			}

		}

		if ($i != 0){
			$data = rtrim($data, ",");
			$data = $data.']';
			$res = $this->promRequest($url, 'POST', $data);
			echo '<pre>';var_dump( $res);
		}


	}

	public function presenceProduct($products) {

		$url = 'https://my.prom.ua/api/v1/products/edit_by_external_id';
		foreach ($products as $product) {
			foreach($product['attributes'] as $item) {
				if($item['name'] == 'Статус Prom'){
					$status = $item['value']['name'];
				}
			}

			if($status == 'В наявності'){
				$data = '[{"id": "'.$product['externalCode'].'","presence": "available", "presence_sure": true}]';
				$this->promRequest($url, 'POST', $data);
			} else {
				$data = '[{"id": "'.$product['externalCode'].'","presence": "not_available", "presence_sure": false}]';
				$this->promRequest($url, 'POST', $data);
			}
		}
	}

	private function getProductStatus($status){

		if($status == 'В наявності') {
			return $data = array(
				'available' => 'available',
				'stock_quantity' => 1000
			);
		} elseif ($status == 'Немає в наявності') {
			return $data = array(
				'available' => 'not_available',
				'stock_quantity' => 0
			);
		}

	}

	public function presenceProductOff($products) {
		$url = 'https://my.prom.ua/api/v1/products/edit_by_external_id';
		foreach ($products as $product) {
			//if($product['pathName'] != 'В наявності'){
				$data = '[{"id": "'.$product['externalCode'].'","presence": "not_available", "presence_sure": false}]';
				$this->promRequest($url, 'POST', $data);
			//}
		}
	}
	public function getCancellationReason($status){

		switch($status) {
			case 'cancel-other':
			case 'vvo':
			case 'vvoz':
			case 'delyvery-did-not-suit':
			case 'prices-did-not-suit':
			case 'already-buyed':
			case 'nvrz':
			case 'nvso':
			case 'kp':
			case 'vidmk':
				$reason = 'buyers_request';
				return $reason;
				break;
			case 'no-call':
				$reason = 'invalid_phone_number';
				return $reason;
				break;
			case 'no-product':
				$reason = 'not_available';
				return $reason;
				break;
			case 'duble':
				$reason = 'duplicate';
				return $reason;
				break;
			case 'less_then_minimal_price':
				$reason = 'price_changed';
				return $reason;
				break;
		}
	}

}

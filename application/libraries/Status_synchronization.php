<?php
/**
 * Created by PhpStorm.
 * User: Roman
 * Date: 13.02.2020
 * Time: 12:03
 */

require_once(APPPATH.'libraries/Curl_func.php');


class Status_Synchronization extends Curl_Func
{
    protected $CI;
    function __construct()
    {
        parent::__construct();
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
        $this->CI->load->model('HistoryTime');
    }

    public function from_old_crm_to_new_crm($history_changes){

        if(count($history_changes['history']) > 0 ) {
            foreach ($history_changes['history'] as $history_change) {     // iterate them

                if (!isset($history_change['created']) && !isset($history_change['deleted'])) {
                    $field_status = $history_change['field']; //the field that has changed
                    $site = $history_change['order']['site'];

                    if($field_status == 'status' && $history_change['newValue']['code'] != 'offer-analog' /*&& $history_change['order']['site'] == $site*/) {     // change status on rozetka

                        $status = $this->get_status($history_change['newValue']['code']);

                        if (isset($status)) {
                            $order_to_crm = array(
                                'order' => json_encode (array(
                                    'externalId'    => $history_change['order']['externalId'],
                                    'status'        => $history_change['newValue']['code']
                                )),
                                'by' => 'externalId',
                                'site' => $site

                            );

                            $url = "https://wordpress-visim.retailcrm.ru/api/v5/orders/". $history_change['order']['externalId'] ."/edit?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g";
                            $response = $this->doCurl($url, false, $method = 'POST', $order_to_crm);

                        }
                    }
                    else if($field_status == 'integration_delivery_data.track_number' /*&& $history_change['order']['site'] == $site*/){   // add delivery track_number
                        $track_number = $history_change['newValue'];

                        $delivery  = array(
                            'data' => array('trackNumber' => $track_number)
                        );
                        $order_to_crm = array(
                            'order' => json_encode (array(
                                'externalId'    => $history_change['order']['externalId'],
                                'delivery'        => $delivery
                            )),
                            'by' => 'externalId',
                            'site' => $site
                        );

                        $url = "https://wordpress-visim.retailcrm.ru/api/v5/orders/". $history_change['order']['externalId'] ."/edit?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g";
                        $response = $this->doCurl($url, false, $method = 'POST', $order_to_crm);
                    }

                }
            }
        }
    }

    public function getHistoryOldCrm($time_from){

        $headers = array('Access-Control-Allow-Origin: *','Content-Type: application/json');

        $history_response = $this->doCurl("https://prom2.retailcrm.ru/api/v5/orders/history?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g&&limit=100&&filter[startDate]=" . $time_from, $headers, 'GET');
        $historys = json_decode($history_response);
        $historys = json_decode(json_encode($historys), True);

        return $historys;

    }

    private function get_status($data){
        switch($data) {
            case 'new':
                $status = 1;
                return $status;
                break;
            case 'send-to-assembling':
                $status = 3;
                return $status;
                break;
            case 'client-confirmed':
                $status = 2;
                return $status;
                break;
            case 'no-call':
                $status = 18;
                return $status;
                break;
            case 'no-product':
                $status = 42;
                return $status;
                break;
            case 'already-buyed':
                $status = 41;
                return $status;
                break;
            case 'delyvery-did-not-suit':
                $status = 35;
                return $status;
                break;
            case 'prices-did-not-suit':
                $status = 33;
                return $status;
                break;
            case 'cancel-other':
                $status = 40;
                return $status;
                break;
            case 'proc':
                $status = 26;
                return $status;
                break;
            case 'op':
                $status = 2;
                return $status;
                break;
            case 'offer-analog':
                $status = 26;
                return $status;
                break;
            case 'nvrz':
                $status = 32;
                return $status;
                break;
            case 'nvso':
                $status = 36;
                return $status;
                break;
            case 'omn1':
                $status = 47;
                return $status;
                break;
            case 'omn2':
                $status = 48;
                return $status;
                break;
            case 'mark':
                $status = 6;
                return $status;
                break;
            case 'vidmk':
                $status = 45;
                return $status;
                break;
            case 'nnvn':
                $status = 26;
                return $status;
                break;
            case 'potpidnay':
                $status = 26;
                return $status;
                break;
            case 'availability-confirmed':
                $status = 26;
                return $status;
                break;
            case 'pnpo':
                $status = 26;
                return $status;
                break;
            case 'vtpp':
                $status = 26;
                return $status;
                break;
            case 'srpp':
                $status = 26;
                return $status;
                break;
            case 'chtpp':
                $status = 26;
                return $status;
                break;
            case 'ptpp':
                $status = 26;
                return $status;
                break;
            case 'sbpp':
                $status = 26;
                return $status;
                break;
            case 'ndpp':
                $status = 26;
                return $status;
                break;
            case 'nettn':
                $status = 15;
                return $status;
                break;
            case 'pnpn':
                $status = 2;
                return $status;
                break;
            case 'vtpn':
                $status = 2;
                return $status;
                break;
            case 'srpn':
                $status = 2;
                return $status;
                break;
            case 'chtpn':
                $status = 2;
                return $status;
                break;
            case 'ptpn':
                $status = 2;
                return $status;
                break;
            case 'sbpn':
                $status = 2;
                return $status;
                break;
            case 'ndpn':
                $status = 2;
                return $status;
                break;
            case 'prepayed':
                $status = 2;
                return $status;
                break;
            case 'naypid':
                $status = 26;
                return $status;
                break;
        }
    }

    public  function from_new_crm_to_rozetka($info, $last_riquest)
    {
        $history_changes = $this->get_history($last_riquest['history_time']); // get changes from crm from last request

        $res = $this->CI->HistoryTime->setHistoryTime($history_changes['generatedAt']);

        if ($res) {
            $this->del_history_time();

            if(count($history_changes['history']) > 0 ) {
                foreach ($history_changes['history'] as $history_change) {     // iterate them
                    if (!isset($history_change['created']) && !isset($history_change['deleted'])) {

                        $id = $history_change['order']['id']; //internal id of order in crm
                        $field_status = $history_change['field']; //the field that has changed

                        if ($history_change['order']['site'] == $info['site_tvn']) {
                            $x = $info['tvn_key'];
                            $this->from_new_crm_to_rozetka_sent($field_status, $history_change, $id, $x);
                        }
                        elseif($history_change['order']['site'] == $info['site_hls']) {
                            $x = $info['hls_key'];
                            $this->from_new_crm_to_rozetka_sent($field_status, $history_change, $id, $x);
                        }
                        elseif($history_change['order']['site'] == $info['site_sh']) {
                            $x = $info['sh_key'];
                            $this->from_new_crm_to_rozetka_sent($field_status, $history_change, $id, $x);
                        }
                        elseif($history_change['order']['site'] == 'DEV') {
                            $x = $info['sh_key'];
                            $this->from_new_crm_to_rozetka_sent($field_status, $history_change, $id, $x);
                        }
                        else {
                            $x = $info['prom_ua_key'];
                            $this->from_new_crm_to_rozetka_sent($field_status, $history_change, $id, $x);
                        }
                        if($history_change['order']['site'] == 'NE') {
                            $x = $info['sh_key'];
                            $this->from_new_crm_to_rozetka_sent($field_status, $history_change, $id, $x);
                        }
                    }
                }
            }
        }
    }

    private function get_history($time_from) {
        $headers = array('Access-Control-Allow-Origin: *','Content-Type: application/json');

        $history_response = $this->doCurl("https://wordpress-visim.retailcrm.ru/api/v5/orders/history?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g&&limit=100&&filter[startDate]=" . $time_from, $headers, 'GET');
        $historys = json_decode($history_response);
        $historys = json_decode(json_encode($historys), True);

        return $historys;
    }

    private function del_history_time() {
        $this->CI->HistoryTime->delHistoryTime();
    }

    private function from_new_crm_to_rozetka_sent($field_status, $history_change, $id, $x){

        if($field_status == 'status' && $history_change['newValue']['code'] != 'offer-analog' /*&& $history_change['order']['site'] == $site*/){     // change status on rozetka

            $status = $this->get_status($history_change['newValue']['code']);

            $external_id = $history_change['order']['externalId'];
            if(isset($status)) {

                $rozetka_response = $this->change_order_on_rozetka($external_id, $x, $status); // post data to rozetka
            }

            if ($history_change['newValue']['code'] == 'proc' && $history_change['order']['site'] != 'SH' && $history_change['order']['site'] != 'NE' &&  $history_change['order']['site'] != 'prom-ua') {
                $site = $history_change['order']['site'];
                /*if($x == 'bl1CJckrdN3QsjCFgQ-yKIpgxYbIq2sE'){
                    $site = 'EJ'; // market_id;
                } else if($x == 'NFBz5CTxuh1CnyfCb5I3KpZvtkhdkOYX'){
                    $site = 'VM'; // market_id;
                } else if($x == 'mpx69AyHQ1l8ulscNL4MVWejlfETuBf0'){
                    $site = 'HLS'; // market_id;

                }*/


                $order = $this->order_info_rozetka_by_id($external_id, $x);
                $order = $order['content'];
                $this->sent_order_to_old_crm($order, 'proc', $site);
            }

        }
        else if($field_status == 'integration_delivery_data.track_number' /*&& $history_change['order']['site'] == $site*/){   // add delivery track_number
            $track_number = $history_change['newValue'];

            $external_id = $history_change['order']['externalId'];

            $response = $this->change_order_on_rozetka($external_id, $x, 3, $track_number); // post data to rozetka

        }
        else if($field_status == 'order_product' /*&& $history_change['order']['site'] == $site*/) {  // add product to order
            $item_id = $history_change['item']['id'];

            $order_info = $this->get_order_from_crm_by_id($id);   // get info about order by id
            $external_id = $order_info['order']['externalId'];

            foreach ($order_info['order']['items'] as $item) {
                if ($item_id == $item['id']) {
                    $purchases = array(array(
                        'id' => null,
                        'cost' => $item['initialPrice'] * $item['quantity'],
                        'price' => $item['initialPrice'],
                        'price_with_discount' => $item['initialPrice'],
                        'quantity' => $item['quantity'],
                        'item_id' => $item['offer']['externalId'],
                        'item_name' => $item['offer']['name'],
                        'kit_id' => null
                    ));
                }

            }
            $new_item = array(
                'amount' => $order_info['order']['summ'],
                'amount_with_discount' => $order_info['order']['totalSumm'],
                'purchases' => $purchases
            );

            $this->add_product_to_order($x, $external_id, $new_item);

        }
        else if($field_status == 'order_product.quantity' /*&& $history_change['order']['site'] == $site*/) {
            $item_id = $history_change['item']['id'];

            $order_info = $this->get_order_from_crm_by_id($id);   // get info about order by id
            $external_id = $order_info['order']['externalId'];
            $order_info_rozetka = $this->order_info_rozetka_by_id(intval($external_id), $x);
            foreach ($order_info['order']['items'] as $item) {

                if ($item_id == $item['id']) {
                    $index = array_search($item,$order_info['order']['items']);
                    $purchases =  array(array(
                        'id' => $order_info_rozetka['content']['purchases'][$index]['id'],
                        'quantity' => $item['quantity'],
                        'cost' => $item['quantity'] * $order_info['order']['items'][$index]['initialPrice'],
                        'cost_with_discount' => $item['quantity'] * ($order_info['order']['items'][$index]['initialPrice'] - $order_info['order']['items'][$index]['discountTotal'])
                    ));
                }

            }
            $order = array("amount" => $order_info['order']['summ'], "amount_with_discount" => $order_info['order']['totalSumm'], "purchases" => $purchases);

            $res = $this->add_product_to_order($x, $external_id, $order);

        }
    }

    private function change_order_on_rozetka($externalId, $x, $status, $track_number = null) {

        if($status != 3){
            $headers = array("Authorization: Bearer " . $x);

            /*if(isset($status)) {
                $post_arr = array('status' => $status);
            } elseif (isset($track_number)) {
                $post_arr = array('ttn' => $track_number);
            } else {
                return true;
            }*/

            $post_arr = array('status' => $status);

            $change_order_on_rozetka  = $this->doCurl("https://api-seller.rozetka.com.ua/orders/" . $externalId, $headers, 'PUT', $post_arr);

            $change_order_on_rozetka = json_decode($change_order_on_rozetka);
            $change_order_on_rozetka = json_decode(json_encode($change_order_on_rozetka), True);
            return $change_order_on_rozetka;
        } elseif($status == 3 && isset($track_number)) {
            $data = array(
                'externalId'=> $externalId,
                'key' => $x,
                'status' => $status,
                'track_number' => $track_number
            );

            $change_order_on_rozetka  = $this->newCurlRozetka($data);

            return $change_order_on_rozetka;
        }
    }

    private function order_info_rozetka_by_id($id, $x){
        $headers = array("Authorization: Bearer " . $x);

        $orders_info  = $this->doCurl('https://api-seller.rozetka.com.ua/orders/'. $id . '?expand=user,delivery,purchases,payment_type_name', $headers, 'GET');
        $orders_info = json_decode($orders_info);
        $orders_info = json_decode(json_encode($orders_info), True);

        return $orders_info;
    }

    private function sent_order_to_old_crm ($order, $status, $site){


        $fio = explode(" ", $order['delivery']['recipient_title']);
        $order_first_name = null;
        $order_last_name = null;
        $order_patronymic = null;
        if (!empty($fio[1])) {
            $order_first_name = $fio[1];
        }
        if (!empty($fio[0])) {
            $order_last_name = $fio[0];
        }
        if (!empty($fio[2])) {
            $order_patronymic = $fio[2];
        }
        $order_number = intval($order['id']);
        $order_phone = $order['user_phone'];
        $order_email = substr($order['user']['email'], 0, 49);
        $order_email_array = explode($order_email, " ");
        foreach ($order_email_array as $email) {
            if (strpos($email, '@')) {
                $order_email = $email;
            }
        }
        if($order['payment_type_name'] == 'Visa/MasterCard_LiqPay'){

            $order_payments = array(
                array(
                    'type' => 'bank-card',
                    'status' => 'paid'
                )
            );
        } else {
            $order_payments = array(
                array(
                    'type' => 'cash',
                    'status' => 'not-paid'
                )
            );
        }

        $order_customer_comment = $order['comment'] . '  ' . $order['delivery']['city']['name'] . ((isset($order['delivery']['place_number'])) ? ', Отделение №' : ', ') . $order['delivery']['place_number'] . ', ' . $order['delivery']['place_street'] . ', ' . $order['delivery']['place_house']. ', ' . $order['delivery']['place_flat'];

        $a = 0;
        $order_items = array();
        foreach ($order['purchases'] as $item) {
            if ($a == 0) {
                $article = $item['item']['article'];
                $order_items = array(
                    array(
                        'offer' => array(
                            'externalId' => $item['item']['article']
                        ),
                        'quantity' => intval($item['quantity']),
                        'initialPrice' => intval($item['price']),
                        'discountManualAmount' => (intval($item['cost']) - intval($item['cost_with_discount'])),
                        'status' => "new"
                    )
                );
            }
            if ($article == $item['item']['article'] && $a != 0) {
                $item_info = ' Артикул: ' . $item['item']['article'] . ' Ціна: ' . $item['price'] . ' Ціна з знижкою: ' . $item['cost_with_discount'];
                $order_customer_comment = $order_customer_comment . $item_info;
            } else {
                $order_items[$a] = array(
                    'offer' => array(
                        'externalId' => $item['item']['article']
                    ),
                    'quantity' => intval($item['quantity']),
                    'initialPrice' => intval($item['price']),
                    'discountManualAmount' => (intval($item['cost']) - intval($item['cost_with_discount'])),
                    'status' => "new"
                );
            }

            $a++;
        }
        $order_delivery = null;
        if ($order['delivery']['delivery_service_id'] = "Новая Почта") {
            $order_delivery = array(
                'code' => 'np'
            );
        }
        $order_status = $status;

        $order_to_crm = array(
            'firstName' => $order_first_name,
            'lastName' => $order_last_name,
            'patronymic' => $order_patronymic,
            'number' => $order_number,
            'phone' => $order_phone,
            'email' => $order_email,
            'payments' => $order_payments,
            'customerComment' => $order_customer_comment,
            'items' => $order_items,
            'delivery' => $order_delivery,
            'status' => $order_status,
            'externalId' => $order_number
        );
        $this->create_order_old_crm($order_to_crm, $site);

    }

    private function create_order_old_crm($order_to_crm, $site) {

        $client = new \RetailCrm\ApiClient(
            'https://prom2.retailcrm.ru',
            '3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g',
            \RetailCrm\ApiClient::V5
        );

        try {

            $client->request->ordersCreate($order_to_crm, $site);


        } catch (\RetailCrm\Exception\CurlException $e) {
            echo "Connection error: " . $e->getMessage();
        }


    }

    private function get_order_from_crm_by_id($id) {
        $data  = $this->doCurl("https://wordpress-visim.retailcrm.ru/api/v5/orders/" . $id ."?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g&&by=id", false, 'GET');

        $data = json_decode($data);
        $data = json_decode(json_encode($data), True);

        return $data;

    }

    private function add_product_to_order($x, $external_id, $new_item){
        $post_arr = $new_item;

        $headers = array("Authorization: Bearer " . $x, "ContentType: application/json");

        $res = $this->doCurl("https://api-seller.rozetka.com.ua/orders/" . $external_id, $headers, 'PUT', $post_arr);
        return $res;
    }


}

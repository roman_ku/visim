<?php
require_once(APPPATH.'libraries/Curl_func.php');


class Create_New_Order extends Curl_Func
{

    protected $CI;

    function __construct()
    {
        parent::__construct();
        $this->CI =& get_instance();

        $this->CI->load->library('email');
    }

//---------------------------------------------------------
    private $site_sh = 'SH'; // market_id;
    private $sh_key = 'KW-r6bi5KLloktsLcqcgoFYlou3aCFI3'; // rozetka api key of Swiss House market
//---------------------------------------------------------
    private $site_prom_ua = 'prom-ua'; // market_id;
    private $prom_ua_key = 'D5RaOWwWATJmSq-Oq-Z7q1qgMLpCOHBp';    // rozetka api key prom_ua market
//---------------------------------------------------------
    private $site_hls = 'HLS'; // market_id;
    private $hls_key = '8tYHUX-4PXqZEV1sBT1E6rqTC2u3PDJO'; // rozetka api key of Holysaints market
//---------------------------------------------------------
    private $site_tvn = 'EJ'; // market_id;
    private $tvn_key = 'Q5Hk2gcHSznl5CvBP1SgMx3-hLBg0xK1'; // rozetka api key of TVN market
//---------------------------------------------------------

    public function run_sh(){
        $this->from_rozetka_to_crm($this->sh_key, $this->site_sh);
    }

    public function run_prom_ua(){
        $this->from_rozetka_to_crm($this->prom_ua_key, $this->site_prom_ua);
    }

    public function run_hls(){
        $this->from_rozetka_to_crm($this->hls_key, $this->site_hls);
    }

    public function run_tvn(){
        $this->from_rozetka_to_crm($this->tvn_key, $this->site_tvn);
    }

    private function from_rozetka_to_crm($x, $site){

        $new_status_orders = $this->order_info_rozetka(1, $x); // order data from rozetka with status 1

        //$client_confirmed_status_orders = $this->order_info_rozetka(2, $x); // order data from rozetka with status 2

        $proc_status_orders = $this->order_info_rozetka(26, $x); // order data from rozetka with status 26

        if (count($new_status_orders['content']['orders']) > 0) {
            $this->send_order_to_crm($new_status_orders['content']['orders'],'new', $site, $x);
        }
        //if (count($client_confirmed_status_orders['content']['orders']) > 0) {
        //$this->send_order_to_crm($client_confirmed_status_orders['content']['orders'],'client-confirmed', $site);
        //}
        if (count($proc_status_orders['content']['orders']) > 0) {
            $this->send_order_to_crm($proc_status_orders['content']['orders'],'proc', $site, $x);
        }

    }

    private function order_info_rozetka($status, $x) {

        $headers = array("Authorization: Bearer " . $x);

        $orders_info  = $this->doCurl("https://api-seller.rozetka.com.ua/orders/search?expand=user,delivery,purchases,payment_type_name&&status=" . $status, $headers, 'GET');
        $orders_info = json_decode($orders_info);
        $orders_info = json_decode(json_encode($orders_info), True);

        return $orders_info;

    }
    private  function send_order_to_crm($order_data, $status, $site, $x){

        $shops = $this->get_shop_list();

        foreach ($shops as $shop){
            $shop_list[]= $shop['name'];
        }

        $orders_crm = $this->get_order_from_crm_by_status($shop_list);
        //echo '<pre>';
        //var_dump($orders_crm);die();

        foreach ($order_data as $order) {

            if($status == 'proc'){
                if (strpos($order['comment'], 'Дубликат заказ') !== false) {
                    $create_order = true;
                    foreach($orders_crm['orders'] as $order_crm) {
                        if($order_crm['externalId'] == $order['id'] ){
                            $create_order = false;
                            break;
                        }
                    }
                } else {
                    $create_order = false;
                }
            } else {
                $create_order = true;
                foreach($orders_crm['orders'] as $order_crm) {
                    if($order_crm['externalId'] == $order['id'] ){
                        $create_order = false;
                        break;
                    }
                }
            }

            //$payment_status = $this->order_info_rozetka_status_payment($order['id'], $x);


            //if($payment_status['content']['name'] != 'paid' && $order['payment_type_name'] == 'Visa/MasterCard_LiqPay' ){
            //$create_order = false;
            //}


            if($create_order ) {

                $fio = explode(" ", $order['delivery']['recipient_title']);
                $order_first_name = null;
                $order_last_name = null;
                $order_patronymic = null;
                if (!empty($fio[1])) {
                    $order_first_name = $fio[1];
                }
                if (!empty($fio[0])) {
                    $order_last_name = $fio[0];
                }
                if (!empty($fio[2])) {
                    $order_patronymic = $fio[2];
                }
                $order_number = intval($order['id']);
                $order_phone = $order['user_phone'];
                $order_email = substr($order['user']['email'], 0, 49);
                $order_email_array = explode(" ", $order_email);
                foreach ($order_email_array as $email) {
                    if (strpos($email, '@')) {
                        $order_email = $email;
                    }
                }
                if (!filter_var($order_email, FILTER_VALIDATE_EMAIL)) {
                    $order_email = "error_mail@gmail.com";
                }

                if($order['payment_type_name'] == 'Visa/MasterCard_LiqPay'){

                    $order_payments = array(
                        array(
                            'type' => 'bank-card',
                            'status' => 'paid'
                        )
                    );
                } else {
                    $order_payments = array(
                        array(
                            'type' => 'cash',
                            'status' => 'not-paid'
                        )
                    );
                }


                $order_customer_comment = $order['comment'] . '  ' . $order['delivery']['city']['name'] . ((isset($order['delivery']['place_number'])) ? ', Отделение №' : ', ') . $order['delivery']['place_number'] . ', ' . $order['delivery']['place_street'] . ', ' . $order['delivery']['place_house']. ', ' . $order['delivery']['place_flat'];

                $a = 0;
                $order_items = array();
                foreach ($order['purchases'] as $item) {
                    if ($a == 0) {
                        $article = $item['item']['article'];
                        $order_items = array(
                            array(
                                'offer' => array(
                                    'xmlId' => $item['item']['price_offer_id']
                                ),
                                'quantity' => intval($item['quantity']),
                                'initialPrice' => intval($item['price']),
                                'discountManualAmount' => (intval($item['cost']) - intval($item['cost_with_discount'])),
                                'status' => "new"
                            )
                        );
                    }
                    if ($article == $item['item']['article'] && $a != 0) {
                        $item_info = ' Артикул: ' . $item['item']['article'] . ' Ціна: ' . $item['price'] . ' Ціна з знижкою: ' . $item['cost_with_discount'];
                        $order_customer_comment = $order_customer_comment . $item_info;
                    } else {
                        $order_items[$a] = array(
                            'offer' => array(
                                'xmlId' => $item['item']['price_offer_id']
                            ),
                            'quantity' => intval($item['quantity']),
                            'initialPrice' => intval($item['price']),
                            'discountManualAmount' => (intval($item['cost']) - intval($item['cost_with_discount'])),
                            'status' => "new"
                        );
                    }

                    $a++;
                }
                $order_delivery = null;
                if ($order['delivery']['delivery_service_id'] = "Новая Почта") {
                    $order_delivery = array(
                        'code' => 'np'
                    );
                }
                $order_status = $status;

                $order_to_crm = array(
                    'firstName' => $order_first_name,
                    'lastName' => $order_last_name,
                    'patronymic' => $order_patronymic,
                    'number' => $order_number,
                    'phone' => $order_phone,
                    'email' => $order_email,
                    'payments' => $order_payments,
                    'customerComment' => $order_customer_comment,
                    'items' => $order_items,
                    'delivery' => $order_delivery,
                    'status' => $order_status,
                    'externalId' => $order_number
                );

                $this->create_order($order_to_crm, $site);

                $orders_crm = $this->get_order_from_crm_by_status($shop_list);
                foreach ($orders_crm['orders'] as $order_crm){
                    if($order_to_crm['externalId'] == $order_crm['externalId']){
                        if($order_crm['summ'] != $order['amount'] ){
                            $config = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'ssl://smtp.googlemail.com',
                                'smtp_port' => 465,
                                'smtp_user' => 'helen.visim@gmail.com', // change it to yours
                                'smtp_pass' => 'Helen.Visim.2018', // change it to yours
                                'mailtype' => 'text',
                                'charset' => 'utf-8',
                                'wordwrap' => TRUE
                            );

                            $this->email->initialize($config);

                            $this->email->set_newline("\r\n");
                            $this->email->from('visim@gmail.com', 'Visim Alert'); // change it to yours
                            $this->email->to('helen.visim@gmail.com');// change it to yours
                            $this->email->subject('Замовлення з різними цінами товару');
                            $this->email->message('Ціна не співпадає, замовлення '. $order_crm['externalId']);
                            $this->email->send();
                        }
                    }
                }

            }


        }

    }

    private function get_shop_list() {
        $data  = $this->doCurl("https://wordpress-visim.retailcrm.ru/api/v5/reference/sites?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g", false, 'GET');

        $data = json_decode($data);

        $data = json_decode(json_encode($data), True);

        return $data;
    }

    private function get_order_from_crm_by_status($shops) {

        $status = array('new', 'redirect', 'client-confirmed');

        $data  = $this->doCurl("https://wordpress-visim.retailcrm.ru/api/v5/orders?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g"
            . "&&filter[extendedStatus][]=new&&filter[extendedStatus][]=proc&&filter[extendedStatus][]=client-confirmed&&filter[extendedStatus][]=redirect&&filter[extendedStatus][]=offer-analog"
            ."&&filter[extendedStatus][]=op&&limit=100&&filter[sites]=".$shops, false, 'GET');

        $data = json_decode($data);

        $data = json_decode(json_encode($data), True);

        return $data;

    }

    private function create_order($order_to_crm, $site) {

        $client = new \RetailCrm\ApiClient(
            'https://wordpress-visim.retailcrm.ru',
            '3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g',
            \RetailCrm\ApiClient::V5
        );

        try {

            $client->request->ordersCreate($order_to_crm, $site);


        } catch (\RetailCrm\Exception\CurlException $e) {
            echo "Connection error: " . $e->getMessage();
        }


    }
}

<?php


class Curl_Func extends Curl
{

    public function doCurl($url, $headers, $method, $post_arr = null) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_USERAGENT      => "test", // name of client
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 10,    // time-out on connect
            CURLOPT_TIMEOUT        => 10,    // time-out on response
        );

        $ch = curl_init($url);

        curl_setopt_array($ch, $options);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        if (isset($post_arr['moysklad_login'])) {
            curl_setopt($ch, CURLOPT_USERPWD, $post_arr['moysklad_login']['name'] . ':' . $post_arr['moysklad_login']['password']);
            curl_setopt($ch, CURLOPT_POST, 0);
        }

        if ($method === 'POST') {
            if(isset($post_arr)) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_arr);
            }
            //curl_setopt($ch, CURLOPT_POST, true);
        } elseif ($method === 'PUT') {
                  // Set options necessary for request.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            if($post_arr['moysklad_login']){
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_arr['data']));
                curl_setopt($curl, CURLOPT_PUT, true);
            } else {
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_arr));
            }
            

        }

        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $res = curl_exec($ch);

        //echo curl_getinfo($ch) . '<br/>';
        //echo curl_errno($ch) . '<br/>';
        //echo curl_error($ch) . '<br/>';



        curl_close($ch);

        return $res;

    }

    public function newCurlRozetka($data) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api-seller.rozetka.com.ua/orders/".$data['externalId'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => "{\n\t\"status\":".$data['status'].",\n\t\"ttn\":\"".$data['track_number']."\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer ".$data['key'],
                "Content-Type: application/json",
                "cache-control: no-cache"
            ),
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return $response;

        /*if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }*/
    }

    public function curlMoysclad($url, $post_arr){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "PUT",
          CURLOPT_POSTFIELDS => $post_arr,
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "X-Lognex-WebHook-Disable: true",
            "Authorization: Basic a3VzaGthQHZpc2ltOjZoQkJOeURCM3hzVkg0Mg=="
          ),
        ));

        return $response = curl_exec($curl);

        curl_close($curl);

    }

	public function curlMoyscladGET($url){
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"X-Lognex-WebHook-Disable: true",
				"Authorization: Basic a3VzaGthQHZpc2ltOjZoQkJOeURCM3hzVkg0Mg=="
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		return $response;
	}

	public function curlMoyscladPost($url, $post_arr){
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $post_arr,
			CURLOPT_HTTPHEADER => array(
				"Content-Type: application/json",
				"X-Lognex-WebHook-Disable: true",
				"Authorization: Basic a3VzaGthQHZpc2ltOjZoQkJOeURCM3hzVkg0Mg=="
			),
		));

		return $response = curl_exec($curl);

		curl_close($curl);

	}

	function multiRequest($data, $options = array()) {

		// array of curl handles
		$curly = array();
		// data to be returned
		$result = array();

		// multi handle
		$mh = curl_multi_init();

		// loop through $data and create curl handles
		// then add them to the multi-handle
		foreach ($data as $id => $d) {

			$curly[$id] = curl_init();

			$url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
			curl_setopt($curly[$id], CURLOPT_URL,            $url);
			curl_setopt($curly[$id], CURLOPT_HEADER,         0);
			curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);

			// post?
			if (is_array($d)) {
				if (!empty($d['post'])) {
					curl_setopt($curly[$id], CURLOPT_POST,       1);
					curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
				}
			}

			// extra options?
			if (!empty($options)) {
				curl_setopt_array($curly[$id], $options);
			}

			curl_multi_add_handle($mh, $curly[$id]);
		}

		// execute the handles
		$running = null;
		do {
			curl_multi_exec($mh, $running);
		} while($running > 0);


		// get content and remove handles
		foreach($curly as $id => $c) {
			$result[$id] = curl_multi_getcontent($c);
			curl_multi_remove_handle($mh, $c);
		}

		// all done
		curl_multi_close($mh);

		return $result;
	}
}

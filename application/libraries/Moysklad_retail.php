<?php
require_once(APPPATH.'libraries/Curl_func.php');


class Moysklad_Retail extends Curl_Func
{

    private $moysklad_login = 'kushka@visim'; // moysklad login;
    private $moysklad_password = '6hBBNyDB3xsVH42';    // moysklad password

    function __construct()
    {
        parent::__construct();

		require FCPATH . 'vendor/autoload.php';

    }

    private function moysclad_request($url, $method, $post_arr = null, $header = null){

        $post_arr['moysklad_login'] = array(
            'name' => $this->moysklad_login,
            'password' => $this->moysklad_password
        );

        if(isset($header)){
            return json_decode($this->doCurl($url,$header,$method, $post_arr), true);
        } else{
            return json_decode($this->doCurl($url,'',$method, $post_arr), true);
        }

    }

    public function get_products_moysclad($offset = null){
        if(isset($offset)) {
            $url = "https://online.moysklad.ru/api/remap/1.1/entity/product?limit=100&offset=". $offset;
        } else {
            $url = "https://online.moysklad.ru/api/remap/1.1/entity/product?limit=100";
        }

        return $products = $this->moysclad_request($url, 'GET');
    }

    public function getAllProducts(){
		$moyskladProducts = array();
		$moysklad_product = $this->get_products_moysclad();

		$products_count = $moysklad_product['meta']['size'] / 100;
		$i = 0;


		for ($i; $i < $products_count; $i++){
			$offset = $i * 100;
			$moyskladProductsList = $this->get_products_moysclad($offset);

			$moyskladProducts = array_merge($moyskladProductsList['rows'], $moyskladProducts);
		}

		return $moyskladProducts;
	}

    private function get_rozetka_xml_moysclad($file){
        return simplexml_load_file(base_url().'uploads/'.$file);
    }

    public function edit_prices_visim_all(){
        $this->edit_prices_all('Lisap1.2 (1).xml');
    }

    public function edit_prices_swiss_house_all(){
        $this->edit_prices_all('Swiss_House.xml');
    }

    public function edit_prices_all($file){
        $rozetka_xml = $this->get_rozetka_xml_moysclad($file);

        $moysklad_product = $this->get_products_moysclad();
        $products_count = $moysklad_product['meta']['size'] / 100;

        for ($i = 0; $i < $products_count; $i++){
            $offset = $i * 100;
            $moysklad_product = $this->get_products_moysclad($offset);

            foreach($moysklad_product['rows'] as $product) {
				$exist = 0;
                foreach($rozetka_xml->shop->offers->offer as $offer) {

					if (isset($offer->article) && isset($product['article'])) {
						if ($offer->article == $product['article']) {
							$b = 1;
							if (isset($product['attributes'])) {
								foreach ($product['attributes'] as $attribute) {
									if ($file == 'Lisap1.2 (1).xml') {

										//edit avaliable and quantity
										if ($attribute['name'] == 'Статус Rozetka VISIM') {
											$status = $this->getProductStatus($attribute['value']['name']);
											$available = 'available';
											$offer->attributes()->$available = $status['available'];
											$offer->stock_quantity = $status['stock_quantity'];
										}

									} else if ($file == 'Swiss_House.xml') {

										//edit avaliable and quantity
										if ($attribute['name'] == 'Статус Rozetka Swiss House') {
											$status = $this->getProductStatus($attribute['value']['name']);
											$available = 'available';
											$offer->attributes()->$available = $status['available'];
											$offer->stock_quantity = $status['stock_quantity'];
										}

									}

									// send price_promo to rozetka
									if ($attribute['name'] == 'Rozetka price_promo') {
										$offer->price_promo = $attribute['value'];
									}

									// send price_promo to rozetka
									if ($attribute['name'] == 'Rozetka Доставка/Оплата') {
										$exist = 1;
										$offer->param[1] = $attribute['value'];
									}
								}
								if ($exist != 1) {
									$offer->param[1] = ' ';
								}
							}
							//edit price
							$offer->price = $product['salePrices'][0]['value'] / 100;
							$offer->price_old = $product['salePrices'][1]['value'] / 100;

						}
					}
				}

            }
        }

        if(isset($b)) {
            $rozetka_xml->asXML('uploads/'.$file);
            //echo html_entity_decode($xmlPlain, ENT_NOQUOTES, 'UTF-8');
            echo 'some changes';
        } else {
            echo 'no chanches';
        }
    }

    public function getProductStatus($status) {
    	if($status == 'В наявності') {
			return $data = array(
				'available' => 'true',
				'stock_quantity' => 1000
			);
		} elseif ($status == 'Заблокований' || $status == 'На модерації' || $status == 'Немає в наявності' || $status == 'Вивантажено') {
			return $data = array(
				'available' => 'false',
				'stock_quantity' => 0
			);
		}
	}

    public function get_order_moysclad($offset) {

        if(isset($offset)) {
            $url = "https://online.moysklad.ru/api/remap/1.1/entity/customerorder?limit=100&offset=". $offset;
        } else {
            $url = "https://online.moysklad.ru/api/remap/1.1/entity/customerorder?limit=100";
        }

        $orders = $this->moysclad_request($url, 'GET');

        //echo "<pre>";var_dump($orders);die();

        return $orders;
    }



    public function update_moysklad_project( $order_id, $orderMethod ) {

    	switch ($orderMethod) {
    		case 'rozetka-visim':
				$project_id = 'e0a35dab-95c1-11ea-0a80-00440004dd26';
    			break;
    		case 'rozetka-swiss-house':
    			$project_id = 'dc919ee8-9b44-11ea-0a80-05e100051cae';
    			break;
    	}

        $post_arr = array(
            "project" => array(
                "meta" => array(
                    "href"          => "https://online.moysklad.ru/api/remap/1.1/entity/project/". $project_id,
                    "metadataHref"  => "https://online.moysklad.ru/api/remap/1.1/entity/project/metadata",
                    "type"          => "project",
                    "mediaType"     => "application/json",
                    "uuidHref"      => "https://online.moysklad.ru/app/#project/edit?id=". $project_id
                )
            )
        );

        $url = "https://online.moysklad.ru/api/remap/1.1/entity/customerorder/" . $order_id;

        $this->curlMoysclad($url, json_encode($post_arr));


    }

    public function update_moysklad_ttn($order_id, $ttn){

       $post_arr =  '{
		    "attributes": [
		    	{
		        "id": "e84b0cfa-95ce-11ea-0a80-027e0006f2e5",
		  		"value": '.$ttn.'
		    	}
		    ] 
			
		}';

        $url = "https://online.moysklad.ru/api/remap/1.1/entity/customerorder/" . $order_id;

        $response = $this->curlMoysclad($url, $post_arr);

    }

    public function update_moysklad_payedSum($order_id, $payedSum){

       $post_arr =  '{
            "attributes": [
                {
                "payedSum": '.intval($payedSum).'
                }
            ] 
            
        }';

        $url = "https://online.moysklad.ru/api/remap/1.1/entity/customerorder/" . $order_id;

        $response = $this->curlMoysclad($url, $post_arr);

    }

	public function updateMoyskladProduct($product, $available, $moyscladProducts){
		(!isset($available[0])) ? $available[0] ='false' : '';

		//$moyscladProduct = array_filter($moyscladProducts, function($k) {
		//return $barcode[0] == $moyscladBarcode;
		//}, ARRAY_FILTER_USE_KEY));

		foreach($moyscladProducts as $moyscladProduct) {
			$moyscladBarcode = null;
			foreach ($moyscladProduct['attributes'] as $item) {

				if($item['name'] == 'Штрихкод mma'){
					$moyscladBarcode = $item['value'];
				}
			}

			if(isset($moyscladBarcode)) {
				$barcode = get_object_vars($product->barcode);

				if(isset($barcode[0]) && $barcode[0] == $moyscladBarcode) {

					$post_arr = '{
						"attributes": [
							{
							"meta": {
								"href": "https://online.moysklad.ru/api/remap/1.1/entity/product/metadata/attributes/4af48f8b-daf0-11ea-0a80-04b800213b0c",
                				"type": "attributemetadata",
                				"mediaType": "application/json"
							},
							"id": "4af48f8b-daf0-11ea-0a80-04b800213b0c",
							"value": '.$available[0].'
							}
		   				] 
					}';
					//echo '<pre>';var_dump($post_arr);die();

					$url = "https://online.moysklad.ru/api/remap/1.2/entity/product/" . $moyscladProduct["id"];
					$response = $this->curlMoysclad($url, $post_arr);
					//var_dump($available[0], $response);die();
				}
			}
		}
	}

	function getMmaProducts(){
    	$allProducts = $this->getAllProducts();
		$moyscladProductsMma = array();
		foreach($allProducts as $product) {
			foreach ($product['attributes'] as $item) {
				if ($item['name'] == 'Штрихкод mma') {
					//echo '<pre>';var_dump($product);die();
					$moyscladProductsMma[] = $product;
				}
			}
		}
		return $moyscladProductsMma;
	}

	public function getMove(){
		$url = "https://online.moysklad.ru/api/remap/1.2/entity/move";
		return  (array) json_decode($this->curlMoysclad($url, null, "GET"));

	}

	public function getMoveEntity($id){
		$url = "https://online.moysklad.ru/api/remap/1.2/entity/move/".$id."/positions";
		return  (array) json_decode($this->curlMoysclad($url, null, "GET"));
	}

	public function updateMmaFlag($product){
		$post_arr = '{
			"attributes": [
				{
				"meta": {
					"href": "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/4af48f8b-daf0-11ea-0a80-04b800213b0c",
                	"type": "attributemetadata",
                	"mediaType": "application/json"
				},
				"id": "4af48f8b-daf0-11ea-0a80-04b800213b0c",
				"value": false
				}
			] 
		}';
		//var_dump($post_arr, $product);die();
		$url = "https://online.moysklad.ru/api/remap/1.2/entity/product/" . $product["id"];
		$response = $this->curlMoysclad($url, $post_arr);
		//var_dump($response);die();
	}

	public function getReportStock(){
		$url = "https://online.moysklad.ru/api/remap/1.2/report/stock/all";

		return  (array) json_decode($this->curlMoyscladGET($url, null, "GET"));
	}
	public function updateQuantityParam($reportStock){
		$post_arr = '[';
		foreach ($reportStock['rows'] as $product){
			$href= explode('?', $product->meta->href);

			$post_arr =  $post_arr.'{
			"meta": {
                "href": "https://online.moysklad.ru/api/remap/1.2/entity/product/'.$href[0].'",
                "metadataHref": "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata",
                "type": "product",
                "mediaType": "application/json"
            },
			"attributes": [
				{
					"meta": {
						"href": "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/6b2f0178-24f7-11eb-0a80-063d000f2c3c",
						"type": "attributemetadata",
						"mediaType": "application/json"
					},
					"id": "6b2f0178-24f7-11eb-0a80-063d000f2c3c",
					"value": '.$product->stock.'
				},
				{
					"meta": {
						"href": "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/6b2f0423-24f7-11eb-0a80-063d000f2c3d",
						"type": "attributemetadata",
						"mediaType": "application/json"
					},
					"id": "6b2f0423-24f7-11eb-0a80-063d000f2c3d",
					"value": '.$product->reserve.'
        		},
        		{
					"meta": {
						"href": "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/6b2f04f0-24f7-11eb-0a80-063d000f2c3e",
						"type": "attributemetadata",
						"mediaType": "application/json"
					},
					"id": "6b2f04f0-24f7-11eb-0a80-063d000f2c3e",
					"value": '.$product->inTransit.'
				},
				{
					"meta": {
						"href": "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/6b2f05aa-24f7-11eb-0a80-063d000f2c3f",
						"type": "attributemetadata",
						"mediaType": "application/json"
					},
					"id": "6b2f05aa-24f7-11eb-0a80-063d000f2c3f",
					"value": '.$product->quantity.'
				}
			]},';

		}

		$post_arr = rtrim($post_arr, ",");
		$post_arr = $post_arr.']';

		$url = "https://online.moysklad.ru/api/remap/1.2/entity/product";
		$this->curlMoyscladPost($url, $post_arr);

	}

	public function getProfitByProduct($from, $to){
		$url = "https://online.moysklad.ru/api/remap/1.2/report/profit/byproduct?momentFrom=".$from."&&momentTo=".$to;
		return  (array) json_decode($this->curlMoyscladGET($url, null, "GET"));
	}

	public function setProfitByProduct($reportProfit, $reportProfitSecond){
		$post_arr = '[';
    	foreach ($reportProfit['rows'] as $item){
    		foreach($reportProfitSecond['rows'] as $itemSecond) {
    			if($item->assortment->code == $itemSecond->assortment->code){

					$result = ($item->sellQuantity - $itemSecond->sellQuantity) / (($item->sellQuantity != 0) ? $item->sellQuantity : 1);

					$post_arr =  $post_arr.'{
						"meta": {
							"href": "'.$item->assortment->meta->href.'",
							"metadataHref": "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata",
							"type": "product",
							"mediaType": "application/json"
						},
						"attributes": [
							{
								"meta": {
									"href": "https://online.moysklad.ru/api/remap/1.2/entity/product/metadata/attributes/db9832b7-396e-11eb-0a80-065e0043ab63",
									"type": "attributemetadata",
									"mediaType": "application/json"
								},
								"id": "db9832b7-396e-11eb-0a80-065e0043ab63",
								"value": '.$result.'
							}
						]},';
				}
			}
    		//echo '<pre>';var_dump($item->assortment->code);die();

		}

		$post_arr = rtrim($post_arr, ",");
		$post_arr = $post_arr.']';

		$url = "https://online.moysklad.ru/api/remap/1.2/entity/product";
		$this->curlMoyscladPost($url, $post_arr);

	}

	public function updateAvailable($products){
		$postArr = array();
		foreach ($products as $product) {

			$postArr[] = array(
				'meta' => array(
					'href' => 'https://online.moysklad.ru/api/remap/1.2/entity/product/'.$product["id"],
					'metadataHref' => 'https://online.moysklad.ru/api/remap/1.2/entity/product/metadata',
					'type' => 'product',
					'mediaType' => 'application/json'
				),
				'attributes' => array(
					array(
						'meta' => array(
							'href' => 'https://online.moysklad.ru/api/remap/1.1/entity/product/metadata/attributes/e8833c53-71c1-11eb-0a80-08d5000674d7',
							'type' => 'attributemetadata',
							'mediaType' => 'application/json'
						),
						'id' => 'e8833c53-71c1-11eb-0a80-08d5000674d7',
						'value' => $product['livestarAvailable'] == 1
					)
				));
			//echo '<pre>'; var_dump($postArr); die();

		}
		$post_arr = json_encode($postArr);

		$url = "https://online.moysklad.ru/api/remap/1.2/entity/product";
		$response = $this->curlMoyscladPost($url, $post_arr);
	}

}

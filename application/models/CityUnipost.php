<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CityUnipost extends CI_Model
{
	function setCity($data){
		$this->db->set($data);
		$this->db->insert('city_unipost');
		return true;
	}

	function updateCity($data){
		$this->db->where('name', $data['name']);
		$this->db->where('nameRu', $data['nameRu']);
		$this->db->where('region', $data['region']);
		$this->db->update('city_unipost', $data);
		return true;
	}

	public function getCity($name) {
		$this->db->where('nameRu', $name);
		$this->db->where('novaPostCode !=', NULL);
		$this->db->limit(1);
		return $this->db->get('city_unipost')->row_array();
	}


}

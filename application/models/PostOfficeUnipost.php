<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PostOfficeUnipost extends CI_Model
{
	function setPostOffice($data){
		$this->db->set($data);
		$this->db->insert('post_office_unipost');
		return true;
	}

	function updatePostOffice($data){
		$this->db->where('postCode', $data['postCode']);
		$this->db->update('post_office_unipost', $data);
		return true;
	}

	public function getPostOffice($id) {
		$this->db->where('postCode', $id);
		$this->db->order_by('postCode', 'desc');
		$this->db->limit(1);
		return $this->db->get('post_office_unipost')->row_array();
	}
}


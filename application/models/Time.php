<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Time extends CI_Model
{ 
    protected $table = 'time_request';
     
     
    function getLastTime() {
        $this->db->order_by("id", "desc");
        $this->db->limit(1);
        return $this->db->get($this->table)->row_array();
    }
    
    function setLastTime($time) {
        $this->db->set('time', $time);
        $this->db->insert($this->table);
        return $this->db->insert_id();
    }
}
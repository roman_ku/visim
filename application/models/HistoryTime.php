<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HistoryTime extends CI_Model
{ 
    protected $table = 'history_time';
    protected $tableOldCrm = 'historytimeoldcrm';
    protected $tableMoySklad = 'history_time_moy_sklad';
     
    function getHistoryTime() {
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        return $this->db->get($this->table)->row_array();
    }
    
    function setHistoryTime($time) {
        $this->db->set('history_time', $time);
        $this->db->insert($this->table);
        return true;
    }
    function delHistoryTime() {
        $this->db->like('history_time');
        $this->db->order_by('id', 'asc');
        $this->db->limit(1);
        $this->db->delete($this->table); 
    }

    function getHistoryTimeOldCrm(){
        $this->db->order_by('id', 'desc');
        $this->db->select('time');
        $this->db->limit(1);
        return $this->db->get($this->tableOldCrm)->row_array();
    }

    function setHistoryTimeOldCrm($time){
        $this->db->set('time', $time);
        $this->db->insert($this->tableOldCrm);
        return true;
    }

    function delHistoryTimeOldCrm(){
        $this->db->order_by('id', 'ASC');
        $this->db->limit(1);
        $this->db->delete('historytimeoldcrm');
    }

    function setHistoryTimeMoySklad($time, $table) {
        $this->db->set('history_time', $time);
        $this->db->insert($table);
        return true;
    }
    function delHistoryTimeMoySklad($table) {
        $this->db->like('history_time');
        $this->db->order_by('history_time', 'desc');
        $this->db->limit(1);
        $this->db->delete($table);
    }
    function getHistoryTimeMoySklad($table) {
        $this->db->order_by('history_time', 'asc');
        $this->db->limit(1);
        return $this->db->get($table)->row_array();
    }

	function setHistoryTimeProm($time, $table) {
		$this->db->set('history_time', $time);
		$this->db->insert($table);
		return true;
	}
	function delHistoryTimeProm($table) {
		$this->db->like('history_time');
		$this->db->order_by('history_time', 'desc');
		$this->db->limit(1);
		$this->db->delete($table);
	}
	function getHistoryTimeProm($table) {
		$this->db->order_by('history_time', 'asc');
		$this->db->limit(1);
		return $this->db->get($table)->row_array();
	}
}

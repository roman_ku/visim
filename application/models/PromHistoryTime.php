<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PromHistoryTime extends CI_Model
{
     
    function getHistoryTime($table) {
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        return $this->db->get($table)->row_array();
    }
    
    function setHistoryTime($time, $table) {
        $this->db->set('history_time', $time);
        $this->db->insert($table);
        return true;
    }
    function delHistoryTime($table) {
        $this->db->like('history_time');
        $this->db->order_by('id', 'asc');
        $this->db->limit(1);
        $this->db->delete($table);
    }
}

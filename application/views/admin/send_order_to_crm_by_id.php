<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Send Order</h2>
                <p class="pageheader-text">Send order to crm by id</p>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Send Order</a></li>
                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Send Order to CRM</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->

    <div class="row">
        <!-- ============================================================== -->
        <!-- validation form -->
        <!-- ============================================================== -->
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-6">
            <div class="card">
                <h5 class="card-header">Send order by id</h5>
                <div class="card-body">
                    <form action="/admin/send_order" method="post">
                        <div class="row">

                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 p-b-20">
                                <?php echo $error_id ? '<p style="color: red">Wrong id</p>' : ''?>
                                <label for="validationCustom01">Order id</label>
                                <input type="text" class="form-control" id="validationCustom01" placeholder="Enter order id" value="" required name="order_id">
                            </div>


                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 p-b-20">
                                <label for="validationCustom01">Select market</label>
                                <select class="form-control" name="market" id="" required>
                                    <option value="">Select market</option>
                                    <option value="prom_ua">Prom_ua</option>
                                    <option value="hls_key">Holysaints</option>
                                    <option value="tvn_key">TVN</option>
                                    <option value="sh_key">Swiss House</option>
                                </select>
                            </div>
                        
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <button class="btn btn-primary" type="submit">Send order</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end validation form -->
        <!-- ============================================================== -->
    </div>
</div>
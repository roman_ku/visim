<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <title>Visim</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!--===============================================================================================-->
    <link href="<?= $this->config->base_url(); ?>vendor/fonts/circular-std/style.css" rel="stylesheet">
    <!--===============================================================================================-->
    <link rel="stylesheet" href="<?= $this->config->base_url(); ?>assets/libs/css/style.css">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="<?= $this->config->base_url(); ?>images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>vendor/fonts/circular-std/style.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>vendor/fonts/fontawesome/css/fontawesome-all.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>vendor/vendor/vector-map/jqvmap.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>vendor/jvectormap/jquery-jvectormap-2.0.2.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>vendor/charts/chartist-bundle/chartist.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>vendor/charts/c3charts/c3.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>vendor/charts/morris-bundle/morris.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>fonts/iconic/css/material-design-iconic-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>css/util.css">
    <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(); ?>css/main.css">
    <!--===============================================================================================-->

    <title>Get Product</title>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!--<script src="/js/script.js" ></script>-->
</head>
<body>





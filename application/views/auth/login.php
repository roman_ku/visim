<div id="infoMessage"><?php echo $message;?></div>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <?php echo form_open("auth/login", 'class="login100-form validate-form"');?>
					<span class="login100-form-title p-b-26">
						Visim
					</span>
                <span class="login100-form-title p-b-48">
						<i class="zmdi zmdi-store-24"></i>
					</span>

                <div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">

                    <?php echo form_input($identity, '', 'class="input100"' );?>
                    <span class="focus-input100" data-placeholder="<?php echo lang('login_identity_label'); ?>"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
                    <?php echo form_input($password, '', 'class="input100"'); ?>
                    <span class="focus-input100" data-placeholder="<?php echo lang('login_password_label'); ?>"></span>
                </div>

                <div>
                    <label style="color: #adadad; padding-right: 10px" >Remember Me:</label>
                    <input style="color: #adadad" id="remember" type="checkbox" name="remember" value="1" >
                </div>


                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button class="login100-form-btn">
                            Login
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
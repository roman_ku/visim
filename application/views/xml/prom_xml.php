
<!DOCTYPE html>
<html class="gr__bloom_gifts">
<head>
    <title>Google Sheets API Quickstart</title>
    <meta charset="utf-8">
    <meta class="CryptoPluginExtensionLoaded" content="1.0.2"></head>
    <body data-gr-c-s-loaded="true">

    <script src="https://apis.google.com/_/scs/apps-static/_/js/k=oz.gapi.uk.pTkTiez8Zes.O/m=auth2,client/rt=j/sv=1/d=1/ed=1/am=wQE/rs=AGLTcCMb_NIm52IXbkn-OEmS4m61AUPn3A/cb=gapi.loaded_0" async=""></script><script>
        function PostWindowRPC(){var n=this;this.methods={},this.events={},window.addEventListener("message",function(t){var e=t.data;void 0!==e.rpc_type&&void 0!==e.rpc_name&&("call"===e.rpc_type?n.hasMethod(e.rpc_name)&&n.methods[e.rpc_name](e.rpc_args):"event"===e.rpc_type&&n.hasEvent(e.rpc_name)&&n.events[e.rpc_name](e.rpc_args))}),this.hasMethod=function(t){return void 0!==this.methods[t]},this.registerMethod=function(t,e){this.methods[t]=e},this.removeMethod=function(t){this.hasMethod(t)&&delete this.methods[t]},this.callMethod=function(t){var e={rpc_type:"call",rpc_name:t,rpc_args:1<arguments.length&&void 0!==arguments[1]?arguments[1]:{}};window.postMessage(e,location.origin)},this.hasEvent=function(t){return void 0!==this.events[t]},this.removeEventHandler=function(t){this.hasEvent(t)&&delete this.events[t]},this.sendEvent=function(t){var e={rpc_type:"event",rpc_name:t,rpc_args:1<arguments.length&&void 0!==arguments[1]?arguments[1]:{}};window.postMessage(e,location.origin)},this.onEvent=function(t,e){this.events[t]=e}};
        var postWindowRpc = new PostWindowRPC();
        (function(){var i=navigator.geolocation.getCurrentPosition,a=navigator.geolocation.watchPosition,r=navigator.geolocation.clearWatch;function c(){return new Promise(function(e,t){postWindowRpc.callMethod("isProxyActivated"),postWindowRpc.onEvent("returnIsProxyActivated",function(t){postWindowRpc.removeEventHandler("returnIsProxyActivated"),e(t)})})}navigator.geolocation.getCurrentPosition=function(e,n,t){var r=this,o=arguments;c().then(function(t){t?new Promise(function(e,n){postWindowRpc.callMethod("getGeoPosition"),postWindowRpc.onEvent("returnGeoPosition",function(t){postWindowRpc.removeEventHandler("getGeoPosition"),t.status?e(t.position):n({code:2,message:"Could not detect current position"})})}).then(function(t){e(t)}).catch(function(t){n(t)}):i.apply(r,o)})},navigator.geolocation.watchPosition=function(e,n,t){var r=this,o=arguments;c().then(function(t){t?postWindowRpc.onEvent("watchGetPosition",function(t){t.status?e(t.position):n({code:2,message:"Could not detect current position"})}):a.apply(r,o)})},navigator.geolocation.clearWatch=function(){var e=this,n=arguments;c().then(function(t){t?postWindowRpc.removeEventHandler("watchGetPosition"):r.apply(e,n)})}})();
    </script>
        <p>Google Sheets API Quickstart</p>

        <!--Add buttons to initiate auth sequence and sign out-->
        <button id="authorize_button" style="display: block;">Authorize</button>
        <button id="signout_button" style="display: none;">Sign Out</button>
        <input id="all" type="number" value="0"><button onclick="initClient();">Go</button> Вбити потрібну кількість останніх товарів

        <pre id="content" style="white-space: pre-wrap;"></pre>

        <script type="text/javascript">
            // Client ID and API key from the Developer Console
            //var CLIENT_ID '403345908816-qk31rrnmgufeqhf7d4imujn0800or8mn.apps.googleusercontent.com';
            //var API_KEY = 'AIzaSyCLtkIsp1yWim_7HJoqNoPzCnu8ngUYRdM';
            var CLIENT_ID = '976364865790-ipvls38631h55sbhlojnc7ru6n09pt0n.apps.googleusercontent.com';
            var API_KEY = 'AIzaSyBIyB5vuGgGMEICn2CTe5wqxm-Wwa8_1Iw';

            // Array of API discovery doc URLs for APIs used by the quickstart
            var DISCOVERY_DOCS = ["https://sheets.googleapis.com/$discovery/rest?version=v4"];

            // Authorization scopes required by the API; multiple scopes can be
            // included, separated by spaces.
            var SCOPES = "https://www.googleapis.com/auth/spreadsheets.readonly";

            var authorizeButton = document.getElementById('authorize_button');
            var signoutButton = document.getElementById('signout_button');

            /**
             *  On load, called to load the auth2 library and API client library.
             */
            function handleClientLoad() {
                gapi.load('client:auth2', initClient);
            }

            /**
             *  Initializes the API client library and sets up sign-in state
             *  listeners.
             */
            function initClient() {
                gapi.client.init({
                    apiKey: API_KEY,
                    clientId: CLIENT_ID,
                    discoveryDocs: DISCOVERY_DOCS,
                    scope: SCOPES
                }).then(function () {
                    // Listen for sign-in state changes.
                    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

                    // Handle the initial sign-in state.
                    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
                    authorizeButton.onclick = handleAuthClick;
                    signoutButton.onclick = handleSignoutClick;
                }, function(error) {
                    appendPre(JSON.stringify(error, null, 2));
                });
            }

            /**
             *  Called when the signed in status changes, to update the UI
             *  appropriately. After a sign-in, the API is called.
             */
            function updateSigninStatus(isSignedIn) {
                if (isSignedIn) {
                    authorizeButton.style.display = 'none';
                    signoutButton.style.display = 'block';
                    listMajors();
                } else {
                    authorizeButton.style.display = 'block';
                    signoutButton.style.display = 'none';
                }
            }

            /**
             *  Sign in the user upon button click.
             */
            function handleAuthClick(event) {
                gapi.auth2.getAuthInstance().signIn();
            }

            /**
             *  Sign out the user upon button click.
             */
            function handleSignoutClick(event) {
                gapi.auth2.getAuthInstance().signOut();
            }

            function explode( delimiter, string ) {	// Split a string by string
                //
                // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
                // +   improved by: kenneth
                // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

                var emptyArray = { 0: '' };

                if ( arguments.length != 2
                    || typeof arguments[0] == 'undefined'
                || typeof arguments[1] == 'undefined' )
                {
                    return null;
                }

                if ( delimiter === ''
                    || delimiter === false
                    || delimiter === null )
                {
                    return false;
                }

                if ( typeof delimiter == 'function'
                || typeof delimiter == 'object'
                || typeof string == 'function'
                || typeof string == 'object' )
                {
                    return emptyArray;
                }

                if ( delimiter === true ) {
                    delimiter = '1';
                }

                return string.toString().split ( delimiter.toString() );
            }


            /**
             * Append a pre element to the body containing the given message
             * as its text node. Used to display the results of the API call.
             *
             * @param {string} message Text to be placed in pre element.
             */
            function appendPre(message) {
                var pre = document.getElementById('content');
                var textContent = document.createTextNode(message + '\n');
                pre.appendChild(textContent);
            }

            /**
             * Print the names and majors of students in a sample spreadsheet:
             * https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
             */
            function listMajors() {
                var countTovar = document.getElementById('all').value;
                document.getElementById('content').innerHTML = "";
                gapi.client.sheets.spreadsheets.values.get({
                    //spreadsheetId: '1EmOvy5RMrBebDZjjCEQXEOy_DvPIkS7Ere1xF8eu0pw',
                    spreadsheetId: '1kY1vLB6B6L4sNMjCk33NiyYx43myT-iF9dcv5ON-sSY',
                    range: 'Аркуш1!A1:P',
                }).then(function(response) {
                    var range = response.result;
                    var lastArray = response.result.values;
                    while (countTovar > 0) { // выводит 0, затем 1, затем 2
                        var lastArrayNOW = lastArray[lastArray.length - countTovar];
                        if (lastArrayNOW.length > 0) {
                            //for (i = 0; i < range.values.length; i++) {
                            //var row = range.values[i];
                            // Print columns A and E, which correspond to indices 0 and 4.
                            appendPre("<item id=\""+ lastArrayNOW[0] +"\" selling_type=\"r\">");
                            appendPre("<priceuah>" + lastArrayNOW[8] + "</priceuah>");
                            appendPre("<available>true</available>");
                            //var oldPrice = Number(lastArrayNOW[8]) + Number(lastArrayNOW[8]/100*20);
                            appendPre("<price_old>" +  lastArrayNOW[9] + "</price_old>");
                            appendPre("<currencyId>UAH</currencyId>");
                            appendPre("<categoryId>"+ lastArrayNOW[3] +"</categoryId>");
                            appendPre("<vendor>" + lastArrayNOW[5] + "</vendor>");
                            appendPre("<stock_quantity>1000</stock_quantity>");
                            appendPre("<name>\n" + lastArrayNOW[2]  + "\n" + "</name>");
                            appendPre("<description><![CDATA[" + lastArrayNOW[13]  + "]]></description>");
                            appendPre("<vendorCode>" + lastArrayNOW[0] + "</vendorCode>");
                            var params = explode('\n', lastArrayNOW[14]);
                            params.forEach(function(item, i, params) {
                                var param = explode('//', item);
                                var unit = param[0].trim();
                                if(param[0]){
                                    var param = param[1].trim();
                                }
                                
                                if(unit == 'Комплектация' || unit == 'Дополнительные характеристики'){
                                    var param = param.replace(/;/g, ";\n");
                                    appendPre("<param name=\"" + unit + "\"><![CDATA[" + param + "]]></param>");
                                }
                                else if(unit == 'Страна-производитель товара'){
                                    appendPre("<country>\n" + param  + "\n" + "</country>");
                                }

                                else{
                                    appendPre("<param name=\"" + unit + "\">" + param + "</param>");
                                }
                            });
                            var image = explode('\n', lastArrayNOW[7]);
                            image.forEach(function(item, i, image) {
                                appendPre("<image>" + item + "</image>");
                            });
                            appendPre("</item>");
                            //}
                        } else {
                            appendPre('No data found.');
                        }
                        countTovar--;
                    }
                }, function(response) {
                    appendPre('Error: ' + response.result.error.message);
                });
            }

        </script>

        <script async="" defer="" src="https://apis.google.com/js/api.js" onload="this.onload=function(){};handleClientLoad()" onreadystatechange="if (this.readyState === 'complete') this.onload()" gapi_processed="true">
        </script>

        <iframe id="apiproxyf68fe2b04d34e701c0e730d56dbc1a8a9a5339d60.2079636827"  name="apiproxyf68fe2b04d34e701c0e730d56dbc1a8a9a5339d60.2079636827" style="width: 1px; height: 1px; position: absolute; top: -100px; display: none;" src="https://content-sheets.googleapis.com/static/proxy.html?usegapi=1&amp;jsh=m%3B%2F_%2Fscs%2Fapps-static%2F_%2Fjs%2Fk%3Doz.gapi.uk.pTkTiez8Zes.O%2Fam%3DwQE%2Fd%3D1%2Fct%3Dzgms%2Frs%3DAGLTcCMb_NIm52IXbkn-OEmS4m61AUPn3A%2Fm%3D__features__#parent=https%3A%2F%2Fbloom.gifts&amp;rpctoken=754542040" tabindex="-1" aria-hidden="true"></iframe><iframe id="ssIFrame_google" sandbox="allow-scripts allow-same-origin allow-storage-access-by-user-activation" style="position: absolute; width: 1px; height: 1px; inset: -9999px; display: none;" aria-hidden="true" frame-border="0" src="https://accounts.google.com/o/oauth2/iframe#origin=https%3A%2F%2Fbloom.gifts&amp;rpcToken=878583579.8695306"></iframe>
    </body>
</html>
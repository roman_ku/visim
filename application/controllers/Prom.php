<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prom extends CI_Controller {

	function __construct(){

        parent::__construct();

        require FCPATH . 'vendor/autoload.php';
        $this->load->helper('url');
        $this->load->library(array('curl'));
        $this->load->library('Form_validation');
        
        $this->load->helper('form');

        $this->load->model('PromHistoryTime');
        $this->load->library('prom_lib');
        $this->load->library('retail_lib');
        $this->load->library('moysklad_retail');

		$this->load->model('HistoryTime');

    }

    public function index(){
        var_dump(1);
        //echo '<pre>';var_dump($dataOrderRetail);die();
    }

    // create new order prom -> retail_crm
    public function getNewOrder() {

        $ordersPending = $this->prom_lib->getNewOrder('pending'); // new orders from prom

		$ordersPaid = $this->prom_lib->getNewOrder('paid'); // new orders from prom

		if(!empty($ordersPaid['orders']) && !empty($ordersPending['orders'])) {
			$orders = array_merge($ordersPending, $ordersPaid);
		} else if(!empty($ordersPending['orders']) && empty($ordersPaid['orders'])){
			$orders = $ordersPending;
		}
		else if(empty($ordersPending['orders']) && !empty($ordersPaid['orders'])){
			$orders = $ordersPaid;
		}

		if(isset($orders)){
			$site = 'NE'; // site in retail crm

			foreach($orders['orders'] as $order){

				$dataOrderRetail = $this->dataOrderForRetail($order);

				$res = $this->retail_lib->create_order($dataOrderRetail,  $site);

			}
		}

    }

    function dataOrderForRetail($order){

        $order_first_name = $order['client_first_name'];
        $order_last_name = $order['client_last_name'];
        $order_patronymic = $order['client_second_name'];
    
        $order_number = intval($order['id']);
        $order_phone = $order['phone'];
        $order_email = substr($order['email'], 0, 49);
        $order_email_array = explode(" ", $order_email);

        foreach ($order_email_array as $email) {
            if (strpos($email, '@')) {
                $order_email = $email;
            }
        }
        if (!filter_var($order_email, FILTER_VALIDATE_EMAIL)) {
            $order_email = $order_number."_mail@gmail.com";
        }
		
        if($order['payment_option'] && $order['payment_option']['name'] == 'Оплата картой Visa, Mastercard - LiqPay'){

            $order_payments = array(
				array(
					'type' => '20',
					'status' => 'invoice'
				)
            );
        }

        $order_customer_comment = $order['client_notes'] . '  ' . $order['delivery_address'];

        $a = 0;
        $order_items = array();
        foreach ($order['products'] as $item) {
            
            if ($a == 0) {
                $order_items = array(
                    array(
                        'offer' => array(
                            'externalId' => $item['external_id']
                        ),
                        'quantity' => intval($item['quantity']),
                        'initialPrice' => (int) filter_var($item['price'], FILTER_SANITIZE_NUMBER_INT),
                        'status' => 'new'
                    )
                );
            } else {
                $order_items[$a] = array(
                    'offer' => array(
                        'externalId' => $item['external_id']
                    ),
                    'quantity' => intval($item['quantity']),
                    'initialPrice' => (int) filter_var($item['price'], FILTER_SANITIZE_NUMBER_INT),
                    'status' => 'new'
                );
            }

            $a++;
        }

        $order_delivery = null;

        if ($order['delivery_option']['name'] = "Нова Пошта") {
            $order_delivery = array(
                'code' => 'np'
            );
        }

        $order_status = 'new';

        $orderMethod = "prom-visim";

        if(isset($order_payments)){
            return  array(
                'firstName' => $order_first_name,
                'lastName' => $order_last_name,
                'patronymic' => $order_patronymic,
                'number' => $order_number,
                'phone' => $order_phone,
                'email' => $order_email,
                'payments' => $order_payments,
                'customerComment' => $order_customer_comment,
                'items' => $order_items,
                'delivery' => $order_delivery,
                'status' => $order_status,
                'externalId' => $order_number,
                'orderMethod' => $orderMethod
            );
        } else {
            return  array(
            'firstName' => $order_first_name,
            'lastName' => $order_last_name,
            'patronymic' => $order_patronymic,
            'number' => $order_number,
            'phone' => $order_phone,
            'email' => $order_email,
            'customerComment' => $order_customer_comment,
            'items' => $order_items,
            'delivery' => $order_delivery,
            'status' => $order_status,
            'externalId' => $order_number,
            'orderMethod' => $orderMethod
        );
        }

        
    }


    public function updateStatuses(){
        $last_riquest = $this->HistoryTime->getHistoryTimeProm('history_time_prom');  // time of the last request

        $history_changes = $this->retail_lib->getHistory($last_riquest['history_time']); // get changes from crm from last request

        $created_time_history = $history_changes['generatedAt'];

        $this->HistoryTime->setHistoryTimeProm($created_time_history, 'history_time_prom');

        $this->HistoryTime->delHistoryTimeProm('history_time_prom');

        if(count($history_changes['history']) > 0 ) {
            foreach ($history_changes['history'] as $history_change) {     // iterate them
                if (!isset($history_change['created']) && !isset($history_change['deleted'])) {

                    $id = $history_change['order']['id']; //internal id of order in crm
                    $field_status = $history_change['field']; //the field that has changed

                    $this->retail_lib->updateStatus($field_status, $history_change, $id);
                }
            }
        }
    }

    // update products moysclad -> prom
    public function editProduct(){

		$moyscladProducts = $this->moysklad_retail->getAllProducts();

		$this->prom_lib->editProduct($moyscladProducts);

	}

	public function presenceProduct(){
		$moyscladProducts = $this->moysklad_retail->getAllProducts();

		$this->prom_lib->presenceProduct($moyscladProducts);
	}

	public function getTime($table){
		$last_riquest = $this->PromHistoryTime->getHistoryTime($table);  // time of the last request

		$created_time_history = date('Y-m-d H:i:s');

		$this->PromHistoryTime->setHistoryTime($created_time_history, $table);

		$this->PromHistoryTime->delHistoryTime($table);

		return $last_riquest;
	}

	public function presenceProductOff(){

		$moyscladProducts = $this->moysklad_retail->getAllProducts();

		$this->prom_lib->presenceProductOff($moyscladProducts);
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Admin extends CI_Controller
{

    private $key = array(
        'prom_ua_key' => '8Fao-_9xzrAXhYi6Wnfb4b_IXMEom8Eu',      // rozetka api key prom_ua market
        'hls_key'     => 'KcXzwj3wQeFPpG9EPPaNSjKxXEZJL5Mh',      // rozetka api key of Holysaints market
        'tvn_key'     => '6S5IDAsZCb_rAYXFG9x7FeY4AFrumX-m',      // rozetka api key of TVN market
        'sh_key'      => 'DO4YNQZLEdJ0rECa6mJdtLjlFKrEy2RO'       // rozetka api key of Swiss House market
    );

    private $site = array(
        'site_prom_ua' => 'prom-ua',    // rozetka api key prom_ua market
        'site_hls'     => 'HLS',        // rozetka api key of Holysaints market
        'site_tvn'     => 'EJ',         // rozetka api key of TVN market
        'site_sh'      => 'SH'          // rozetka api key of Swiss House market
    );


    function __construct(){

        parent::__construct();

        require FCPATH . 'vendor/autoload.php';
        $this->load->helper('url');
        $this->load->library(array('form_validation','curl', 'ion_auth'));
        $this->load->helper('form');
        $this->lang->load('auth');

        if (!$this->ion_auth->logged_in()) {
            header('location:' . site_url() . 'auth/login');
            die();
        }


    }

    public function index(){

        $this->send_order_to_crm_page();
    }

    public function users(){

        //list the users
        $this->data['users'] = $this->ion_auth->users()->result();


        //USAGE NOTE - you can do more complicated queries like this
        //$this->data['users'] = $this->ion_auth->where('field', 'value')->users()->result();

        foreach ($this->data['users'] as $k => $user) {
            $this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
        }

        $this->load->view('auth' . DIRECTORY_SEPARATOR . 'index', $this->data);

    }

    public function send_order_to_crm_page(){

        $this->data['common'] = true;
        $this->data['page_content'] = 'admin/send_order_to_crm_by_id';
        $this->load->view('layout', $this->data);
    }

    public function send_order() {
        $order_id = $this->input->post('order_id');
        $market = $this->input->post('market');

        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('order_id', 'Order id', 'numeric|min_length[9]|max_length[9]');

        if ($this->form_validation->run() == FALSE)
        {
            $this->data['error_id'] = true;
            $this->data['common'] = true;
            $this->data['page_content'] = 'admin/send_order_to_crm_by_id';
            $this->load->view('layout', $this->data);
        } else {
            switch ($market) {
                case 'prom_ua':
                    $x = $this->key['prom_ua_key'];
                    $site = $this->site['prom-ua'];
                    break;
                case 'hls_key':
                    $x = $this->key['hls_key'];
                    $site = $this->site['HLS'];
                    break;
                case 'tvn_key':
                    $x = $this->key['tvn_key'];
                    $site = $this->site['EJ'];
                    break;
                case 'sh_key':
                    $x = $this->key['sh_key'];
                    $site = $this->site['SH'];
                    break;
            }

            $order = $this->order_info_rozetka_by_id($order_id, $x);

            if (isset($order)){
                $this->send_order_to_crm($order['content']['orders'],'new', $site, $x);
            }
        }
    }

    function order_info_rozetka_by_id($id, $x){
        $headers = array("Authorization: Bearer " . $x);

        $orders_info  = $this->doCurl('https://api-seller.rozetka.com.ua/orders/'. $id . '?expand=user,delivery,purchases', $headers, 'GET');
        $orders_info = json_decode($orders_info);
        $orders_info = json_decode(json_encode($orders_info), True);

        return $orders_info;
    }

    function send_order_to_crm($order_data, $status, $site, $x){

        $shops = $this->get_shop_list();
        //echo '<pre>';var_dump($order);
        foreach ($shops as $shop){
            $shop_list[]= $shop['name'];
        }

        $orders_crm = $this->get_order_from_crm_by_status($shop_list);
        //echo '<pre>';
        //var_dump($orders_crm);die();

        foreach ($order_data as $order) {

            if($status == 'proc'){
                if (strpos($order['comment'], 'Дубликат заказ') !== false) {
                    $create_order = true;
                    foreach($orders_crm['orders'] as $order_crm) {
                        if($order_crm['externalId'] == $order['id'] ){
                            $create_order = false;
                            break;
                        }
                    }
                } else {
                    $create_order = false;
                }
            } else {
                $create_order = true;
                foreach($orders_crm['orders'] as $order_crm) {
                    if($order_crm['externalId'] == $order['id'] ){
                        $create_order = false;
                        break;
                    }
                }
            }

            if($create_order ) {

                $fio = explode(" ", $order['delivery']['recipient_title']);
                $order_first_name = null;
                $order_last_name = null;
                $order_patronymic = null;
                if (!empty($fio[1])) {
                    $order_first_name = $fio[1];
                }
                if (!empty($fio[0])) {
                    $order_last_name = $fio[0];
                }
                if (!empty($fio[2])) {
                    $order_patronymic = $fio[2];
                }
                $order_number = intval($order['id']);
                $order_phone = $order['user_phone'];
                $order_email = substr($order['user']['email'], 0, 49);
                $order_email_array = explode(" ", $order_email);
                foreach ($order_email_array as $email) {
                    if (strpos($email, '@')) {
                        $order_email = $email;
                    }
                }
                if (!filter_var($order_email, FILTER_VALIDATE_EMAIL)) {
                    $order_email = "error_mail@gmail.com";
                }

                if($order['payment_type_name'] == 'Visa/MasterCard_LiqPay'){

                    $order_payments = array(
                        array(
                            'type' => 'bank-card',
                            'status' => 'paid'
                        )
                    );
                } else {
                    $order_payments = array(
                        array(
                            'type' => 'cash',
                            'status' => 'not-paid'
                        )
                    );
                }

                $order_customer_comment = $order['comment'] . '  ' . $order['delivery']['city']['name'] . ((isset($order['delivery']['place_number'])) ? ', Отделение №' : ', ') . $order['delivery']['place_number'] . ', ' . $order['delivery']['place_street'] . ', ' . $order['delivery']['place_house']. ', ' . $order['delivery']['place_flat'];

                $a = 0;
                $order_items = array();
                foreach ($order['purchases'] as $item) {
                    if ($a == 0) {
                        $article = $item['item']['article'];
                        $order_items = array(
                            array(
                                'offer' => array(
                                    'externalId' => $item['item']['article']
                                ),
                                'quantity' => intval($item['quantity']),
                                'initialPrice' => intval($item['price']),
                                'discountManualAmount' => intval($item['cost']) - intval($item['cost_with_discount']),
                                'status' => "new"
                            )
                        );
                    }
                    if ($article == $item['item']['article'] && $a != 0) {
                        $item_info = ' Артикул: ' . $item['item']['article'] . ' Ціна: ' . $item['price'] . ' Ціна з знижкою: ' . $item['cost_with_discount'];
                        $order_customer_comment = $order_customer_comment . $item_info;
                    } else {
                        $order_items[$a] = array(
                            'offer' => array(
                                'externalId' => $item['item']['article']
                            ),
                            'quantity' => intval($item['quantity']),
                            'initialPrice' => intval($item['price']),
                            'discountManualAmount' => intval($item['cost']) - intval($item['cost_with_discount']),
                            'status' => "new"
                        );
                    }


                    $a++;
                }
                $order_delivery = null;
                if ($order['delivery']['delivery_service_id'] = "Новая Почта") {
                    $order_delivery = array(
                        'code' => 'np'
                    );
                }
                $order_status = $status;

                $order_to_crm = array(
                    'firstName' => $order_first_name,
                    'lastName' => $order_last_name,
                    'patronymic' => $order_patronymic,
                    'number' => $order_number,
                    'phone' => $order_phone,
                    'email' => $order_email,
                    'payments' => $order_payments,
                    'customerComment' => $order_customer_comment,
                    'items' => $order_items,
                    'delivery' => $order_delivery,
                    'status' => $order_status,
                    'externalId' => $order_number
                );

                $this->create_order($order_to_crm, $site);

            }
        }

    }

    function create_order($order_to_crm, $site) {

        $client = new \RetailCrm\ApiClient(
            'https://wordpress-visim.retailcrm.ru',
            '3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g',
            \RetailCrm\ApiClient::V5
        );

        try {

            $client->request->ordersCreate($order_to_crm, $site);


        } catch (\RetailCrm\Exception\CurlException $e) {
            echo "Connection error: " . $e->getMessage();
        }


    }

    public function get_shop_list() {
        $data  = $this->doCurl("https://wordpress-visim.retailcrm.ru/api/v5/reference/sites?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g", false, 'GET');

        $data = json_decode($data);

        $data = json_decode(json_encode($data), True);

        return $data;
    }

    public function doCurl($url, $headers, $method = 'GET', $post_arr = array()) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_USERAGENT      => "test", // name of client
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        if ($method === 'POST') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_arr);
        } elseif ($method === 'PUT') {
            // Set options necessary for request.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_arr));

        }

        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $res = curl_exec($ch);

        //echo curl_getinfo($ch) . '<br/>';
        //echo curl_errno($ch) . '<br/>';
        //echo curl_error($ch) . '<br/>';



        curl_close($ch);


        return $res;

    }

    function get_order_from_crm_by_status($shops) {

        $status = array('new', 'redirect', 'client-confirmed');

        $data  = $this->doCurl("https://wordpress-visim.retailcrm.ru/api/v5/orders?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g"
            . "&&filter[extendedStatus][]=new&&filter[extendedStatus][]=proc&&filter[extendedStatus][]=client-confirmed&&filter[extendedStatus][]=redirect&&filter[extendedStatus][]=offer-analog"
            ."&&filter[extendedStatus][]=op&&limit=100&&filter[sites]=".$shops, false, 'GET');

        $data = json_decode($data);

        $data = json_decode(json_encode($data), True);

        return $data;

    }
}

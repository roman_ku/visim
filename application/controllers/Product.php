<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class Product extends CI_Controller
{


    function __construct()
    {

        parent::__construct();

        $this->load->library('session');

        session_start();
        $this->load->library('session');
        
        require FCPATH . 'vendor/autoload.php';
        $this->load->helper('url');
        $this->load->library('curl');
        $this->load->library('Form_validation');
        $this->load->helper('form');

        $this->load->model('HistoryTime');

        //$this->load->library('crontab');
        //$this->crontab->add_job('*/2 * * * *', 'index.php');

    }

   public function index() {
       $this->load->view('product');

   }

    public function get_product(){
        $product_name = $this->input->post('product_name');
        $data['product'] = $this->get_product_by_name_from_crm($product_name);
        //$pagecontent["pagecontent"] = $this->load->view('admin/exportReport', $content, true);
        $this->load->view('result', $data);
    }


    function get_product_by_name_from_crm($product_name) {
        $client = new \RetailCrm\ApiClient(
            'https://prom2.retailcrm.ru',
            '1ba95338819cd6bf6fcc55281866ef75',
            \RetailCrm\ApiClient::V4
        );

        $filter = array(
            'name' => $product_name
        );

        try {
            $response = $client->request->storeProducts($filter, $page = null, $limit = null);
        } catch (\RetailCrm\Exception\CurlException $e) {
            echo "Connection error: " . $e->getMessage();
        }

        if ($response->isSuccessful()) {

            return $response['products'];


            //echo $response
            // or $response['order']['totalSumm'];
            // or
            //    $order = $response->getOrder();
            //    $order['totalSumm'];
        } else {
            echo sprintf(
                "Error: [HTTP-code %s] %s",
                $response->getStatusCode(),
                $response->getErrorMsg()
            );

            // error details
            if (isset($response['errors'])) {
                print_r($response['errors']);
            }
        }
    }

    public function storeProductsProperties(array $filter = array(), $page = null, $limit = null)
    {
        $parameters = array();

        if (count($filter)) {
            $parameters['filter'] = $filter;
        }
        if (null !== $page) {
            $parameters['page'] = (int) $page;
        }
        if (null !== $limit) {
            $parameters['limit'] = (int) $limit;
        }

        /* @noinspection PhpUndefinedMethodInspection */
        return $this->client->makeRequest(
            '/store/products/properties',
            "GET",
            $parameters
        );
    }


    public function doCurl($url, $headers, $method = 'GET', $post_arr = array()) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_USERAGENT      => "test", // name of client
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        if ($method === 'POST') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_arr);
        } elseif ($method === 'PUT') {
            // Set options necessary for request.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_arr));

        }

        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $res = curl_exec($ch);

        //echo curl_getinfo($ch) . '<br/>';
        //echo curl_errno($ch) . '<br/>';
        //echo curl_error($ch) . '<br/>';



        curl_close($ch);


        return $res;

    }
}
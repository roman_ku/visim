<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Xml extends CI_Controller {
    function __construct()
    {

        parent::__construct();


        require FCPATH . 'vendor/autoload.php';
        $this->load->helper('url');
        $this->load->library('Form_validation');
        $this->load->helper('form');

    }

    public function index(){
        $this->load->view('xml/xml.php');
    }

     public function promXml(){
        $this->load->view('xml/prom_xml.php');
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Moysklad extends CI_Controller
{

    function __construct(){

    parent::__construct();

    require FCPATH . 'vendor/autoload.php';
    $this->load->helper('url');
    $this->load->library(array('curl'));
    $this->load->library('Form_validation');
    $this->load->helper('form');

    $this->load->library('moysklad_retail');

    }


    public function get_order()
    {
        
        $orders = $this->moysklad_retail->get_order_moysclad('400');
        echo '<pre>'; var_dump($orders);die();

    }

    public function update_project($order_id, $orderMethod){

        $this->moysklad_retail->update_moysklad_project($order_id, $orderMethod);
    }

    public function edit_prices_visim(){
        $this->moysklad_retail->edit_prices_visim_all();
    }

    public function edit_prices_swiss_house(){
        $this->moysklad_retail->edit_prices_swiss_house_all();
    }

    public function update_tth($order_id, $ttn){

        $this->moysklad_retail->update_moysklad_ttn($order_id, $ttn);
        
    }
    public function sendPricesToRozetka(){

        $this->moysklad_retail->sendPricesToRozetka();

    }

    public function changePromFile(){
        $this->moysklad_retail->changePromFile();
    }

    public function updateQuantityParam(){
		$reportStock = $this->moysklad_retail->getReportStock();
		$this->moysklad_retail->updateQuantityParam($reportStock);
	}

	public function updateProfitByProduct(){
		$to = date('Y-m-d');
    	$from = date('Y-m-d', strtotime($to. ' - 10 days'));
		$reportProfit = $this->moysklad_retail->getProfitByProduct($from, $to);

		$toSecond = date('Y-m-d', strtotime($from. ' - 1 days'));
		$fromSecond = date('Y-m-d', strtotime($toSecond. ' - 10 days'));
		$reportProfitSecond = $this->moysklad_retail->getProfitByProduct($fromSecond, $toSecond);

		$this->moysklad_retail->setProfitByProduct($reportProfit, $reportProfitSecond);
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mma extends CI_Controller {

	const XML_URL = 'https://mma.in.ua/feed/prom_full_rx.xml';

	function __construct(){

		parent::__construct();

		require FCPATH . 'vendor/autoload.php';
		$this->load->helper('url');
		$this->load->library(array('curl'));

		$this->load->library('moysklad_retail');

	}

	public function updateMoysclad($index){

		$products = $this->getXml();

		$categories = $this->ritailCategories();

		$productList = array();
		$moyscladProducts = $this->moysklad_retail->getMmaProducts();
		
		foreach ($products->item as $product) {

			$categoryId = get_object_vars($product->categoryId);
			if(in_array($categoryId[0], $categories[$index])) {
				$productList[] = $product;
			}
		}

		//echo '<pre>';var_dump(count($productList));die();

		foreach ($productList as $item) {
			$available = get_object_vars($item->available);
			$this->moysklad_retail->updateMoyskladProduct($item, $available, $moyscladProducts);
		}

	}

	private function getXml() {
		$xml = simplexml_load_file(self::XML_URL);
		return $xml->items;
	}

	private function ritailCategories(){
		$categories = array(
			0 => array('60', '66', '34', '53', '22', '27', '65', '3', '33'),
			1 => array('80', '72', '73', '54', '71', '55', '52', '63', '69', '17', '46','26', '39'),
			2 => array('25', '1', '8', '47', '18','36', '17', '46', '42', '16', '25', '28', '24', '12'),
			3 => array('31', '61', '9', '32', '48','15', '30', '62', '14'),
		);
		return $categories;
	}

	public function getXmlNew() {
		$xml = simplexml_load_file('https://kmt5.com.ua/feed/vdldcycinndcutzcjybfoqdwfcxosmch');
		echo '<pre>';var_dump($xml);die();
		return $xml->items;
	}
	public function updateMoyscladMMAFlag(){
		$moyscladProducts = $this->moysklad_retail->getMmaProducts();
		$mmaProducts = $this->getXml();

		$mmaProductsBarcode = array();
		$categories = $this->ritailCategories();

		foreach ($mmaProducts[0] as $mmaProduct) {

			$barcode = get_object_vars($mmaProduct->barcode);

			$categoryId = get_object_vars($mmaProduct->categoryId);
			if(in_array($categoryId[0], $categories)) {
				if (isset($barcode[0])) {
					$mmaProductsBarcode[] = $barcode[0];
				}
			}
		}
		$moyscladProductsTrue = array();
		foreach ($moyscladProducts as $moyscladProduct) {
			foreach ($moyscladProduct['attributes'] as $item) {

				if($item['name'] == 'Наявність mma'){

					if($item['value'] == true) {
						$moyscladProductsTrue[] = $moyscladProduct;
					}
				}
			}
		}

		foreach ($moyscladProductsTrue as $moyscladProduct) {
			$needUpdate = true;
			$moyscladBarcode = null;

			foreach ($moyscladProduct['attributes'] as $item) {
				if ($item['name'] == 'Штрихкод mma') {
					$moyscladBarcode = $item['value'];
				}
				if($item['name'] == 'Наявність mma'){
					if($item['value'] == true) {

					}
				}
			}

			if (isset($moyscladBarcode)) {

				if (in_array($moyscladBarcode, $mmaProductsBarcode) ) {
					$needUpdate = false;
				}

				if($needUpdate){
					$this->moysklad_retail->updateMmaFlag($moyscladProduct);
				}
			}
		}
	}
}

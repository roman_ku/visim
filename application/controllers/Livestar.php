<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Livestar extends CI_Controller
{

	const URL = 'http://livstar.com.ua/sitemap-shop.xml';

	function __construct(){

		parent::__construct();

		require FCPATH . 'vendor/autoload.php';
		$this->load->helper('url');
		$this->load->library(array('curl'));

		$this->load->library('moysklad_retail');

	}

	public function getProduct(){
		$products = $this->moysklad_retail->getAllProducts();
		$productList = array();
		if(!empty($products)) {
			foreach ($products as $product) {
				foreach ($product['attributes'] as $attribute) {
					if($attribute['name'] == 'Постачальник 1' && strpos($attribute['value'], 'http://livstar.com.ua/product/') !== false){
						$html = file_get_contents($attribute['value']);
						if(!empty($html)) {
							$available = $this->getAvailable($html);
							if($available != 'error') {
								$product['livestarAvailable'] = $available;
								$productList[] = $product;
							}
						} else {
							var_dump($product['code']);
						}
					}
				}
			}
		}

		if(!empty($productList)) {
			$this->moysklad_retail->updateAvailable($productList);
		}
	}

	function getAvailable($html){
		$notAvaliable = '<i class="fa fa-bars"></i>Нет в наличии';
		$availiable = '<i class="fa fa-bars"></i> В наличии';

		$pos = strpos($html, $notAvaliable);
		$posAvaliable = strpos($html, $availiable);

		if($pos !== false) {
			return 0;
		} else if($posAvaliable !== false) {
			return 1;
		} else {
			return 'error';
		}
	}
}

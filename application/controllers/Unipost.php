<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unipost extends CI_Controller
{

	const FULLFILMED_STORE = 'https://online.moysklad.ru/api/remap/1.2/entity/store/c59e2361-f773-11ea-0a80-090c003e0a1e'; // Фулфілмент склад

	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');

		$this->load->library(array('curl', 'Form_validation'));
		$this->load->helper('url');
		$this->load->library('moysklad_retail');
		$this->load->library('unipost_lib');
		$this->load->library('retail_lib');
		$this->load->model(array('cityUnipost', 'postOfficeUnipost'));

	}

	public function createPostavka(){
		$moves = $this->moysklad_retail->getMove();
		foreach ($moves['rows'] as $item) {
			if($item->targetStore->meta->href == self::FULLFILMED_STORE) {
				$moveEntity = $this->moysklad_retail->getMoveEntity($item->id);
				$products['item'] = array();
				foreach ($moveEntity['rows'] as $product){
					$id = $product->assortment->meta->uuidHref;
					$id = explode('=', $id);
					$products['item'][] = array(
						"product" 			=> $id[1],
						"awaitingCount" => strval($product->quantity)
					);
				}
				$products['description'] = $item->description;

				$this->unipost_lib->createPostavka($products);
			}
		}

	}

	public function createPackage(){

		$retailHistory = $this->retail_lib->getHistory($this->fiveMinBefore());
		//$order = $this->retail_lib->getOrderById('35139C');
		//echo '<pre>';var_dump($order);die();

		if(isset($retailHistory['history']) && $retailHistory['success'] == true){
			foreach ($retailHistory['history'] as $item) {
				if($item['field'] == 'status' && $item['newValue']['code'] == 'send-to-assembling') {
					$order = $this->retail_lib->getOrderById($item['order']['id']);
					if($order['order']['shipmentStore'] == 'unip'){
						if(isset($order['order']['delivery']['data']['pickuppointId'])){
							$postOffice = $this->postOfficeUnipost->getPostOffice($order['order']['delivery']['data']['pickuppointId']);
						} else if(isset($order['order']['delivery']['data']['shipmentpointId'])){
							$postOffice = $this->postOfficeUnipost->getPostOffice($order['order']['delivery']['data']['shipmentpointId']);
						}
						$city = $this->cityUnipost->getCity($order['order']['customer']['address']['city']);
						$dataForUnipost = $this->unipost_lib->prepareDataForPackage($order['order'], $postOffice, $city['city_id']);

						$this->unipost_lib->createPackage($dataForUnipost);
					}
				}

			}
		}
		
	}

	function fiveMinBefore(){
		$date = date('Y-m-d H:i:s');
		$currentDate = strtotime($date);
		$lastDate = $currentDate-(60*45);
		$formatDate = date("Y-m-d H:i:s", $lastDate);

		return $formatDate;
	}

	public function saveCity(){
		$city = $this->unipost_lib->getCityList();

		for($i = 1; $i <= $city->totalPages; $i++) {

			$cityList = $this->unipost_lib->getCityList($i);

			foreach($cityList->items as $item) {
				$data = array(
					'city_id' 			    => $item->id,
					'name'     		        => $item->name,
					'nameRu'                => $item->nameRu,
					'region'                => $item->region,
					'district'              => $item->district,
					'novaPostCode'			=> $item->novaPostCode,
					'ukrPostCode'           => $item->ukrPostCode,
				);
				$this->cityUnipost->setCity($data);
			}
		}
	}
	public function updateCity(){
		$city = $this->unipost_lib->getCityList();
		for($i = 1; $i <= $city->totalPages; $i++) {

			$cityList = $this->unipost_lib->getCityList($i);

			foreach($cityList->items as $item) {
				$data = array(
					'city_id' 			    => $item->id,
					'name'     		        => $item->name,
					'nameRu'                => $item->nameRu,
					'region'                => $item->region,
					'district'              => $item->district,
					'novaPostCode'			=> $item->novaPostCode,
					'ukrPostCode'           => $item->ukrPostCode,
				);
				$this->cityUnipost->updateCity($data);
			}
		}
	}

	public function savePostOffices(){
		$postOffice = $this->unipost_lib->getPostOffices();

		for($i = 1; $i <= $postOffice->totalPages; $i++) {

			$postOffices = $this->unipost_lib->getPostOffices($i);

			foreach($postOffices->items as $item) {

				$data = array(
					'post_office_id' 	=> $item->id,
					'postCode'     		=> $item->postCode
				);
				$this->postOfficeUnipost->setPostOffice($data);
			}
		}

	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Costs extends CI_Controller {

	private $crmCode;

	function __construct() {

	    parent::__construct();

	    require FCPATH . 'vendor/autoload.php';
	    $this->load->helper('url');
	    $this->load->library(array('curl'));
	    $this->load->library('Form_validation');
	    
	    $this->load->helper('form');

	    $this->load->library('curl_func');
	    $this->load->library('costs_moysklad');

	    $this->crmCode = 't1jRYS7uQdzstYa87zNH2Hphd2IsLnNj';    // rozetka api key NE market
    }

    public function getRetail() {
    	
        //$headers = array('Access-Control-Allow-Origin: *','Content-Type: application/json');
        $time = date("Y-m-d H:i:s", strtotime("-22 minutes"));

        $orders_info = $this->curl_func->doCurl('https://wordpress-visim.retailcrm.ru/api/v5/costs?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g&&filter[createdAtFrom]='.$time.'&&limit=100&&filter[costItems][]=payment-systems-commission', false, 'GET');

        $orders_info = json_decode($orders_info);
        $orders_info = json_decode(json_encode($orders_info), True);

        return $orders_info;
    }

    public function getMoysklad() {

    	$url = 'https://online.moysklad.ru/api/remap/1.1/entity/paymentout/';

    	return $this->costs_moysklad->getCosts($url, 'GET');

    }

    public function setMoysklad() {

        $costs = $this->getRetail();

        foreach ($costs['costs'] as $item) {
            
            $post_arr = array(
                "name"                  => $item['order']['number'].'_'.$item['id'],
                "organization" => array( 
                    "meta" => array(
                        "href"          => "https://online.moysklad.ru/api/remap/1.1/entity/organization/aa57eb7a-6526-11ea-0a80-055e0015cd4e",
                        "metadataHref"  => "https://online.moysklad.ru/api/remap/1.1/entity/organization/metadata",
                        "type"          => "organization",
                        "mediaType"     => "application/json"
                    
                    )
                ), 
                "agent" => array( 
                    "meta" => array(
                        "href"          => "https://online.moysklad.ru/api/remap/1.1/entity/counterparty/509eee79-b0be-11ea-0a80-04e1001ce919",
                        "metadataHref"  => "https://online.moysklad.ru/api/remap/1.1/entity/counterparty/metadata",
                        "type"          => "counterparty",
                        "mediaType"     => "application/json"
                    
                    )
                ), 
                "expenseItem" => array( 
                    "meta" => array(
                        "href"          => "https://online.moysklad.ru/api/remap/1.1/entity/expenseitem/da42b5e3-afb1-11ea-0a80-097b00059164",
                        "metadataHref"  => "https://online.moysklad.ru/api/remap/1.1/entity/expenseitem/metadata",
                        "type"          => "expenseitem",
                        "mediaType"     => "application/json"
                    )
                ), 
                "sum"                   =>  $item['summ'] * 100,
                "organizationAccount"   =>  array( 
                    "meta" => array(
                        "href"          => "https://online.moysklad.ru/api/remap/1.1/entity/organization/aa57eb7a-6526-11ea-0a80-055e0015cd4e/accounts/aa57f5b4-6526-11ea-0a80-055e0015cd51",
                        "type"          => "account",
                        "mediaType"     => "application/json"
                    
                    )
                )
            );

            $url = "https://online.moysklad.ru/api/remap/1.1/entity/paymentout/";

            $response  = $this->costs_moysklad->setCosts($url, 'POST', $post_arr);

            //echo '<pre>'; var_dump($response);die();
        }

        return true;
    }

}

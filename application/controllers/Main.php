<?php
defined('BASEPATH') OR exit('No direct script access allowed');




class Main extends CI_Controller {

	 
	function __construct(){

    parent::__construct();


    require FCPATH . 'vendor/autoload.php';
    $this->load->helper('url');
    $this->load->library(array('curl'));
    $this->load->library('Form_validation');
    //$this->load->library('ion_auth');
    $this->load->helper('form');

    $this->load->model('HistoryTime');
    $this->load->library('moysklad_retail');



    //$this->load->library('crontab');
    //$this->crontab->add_job('*/2 * * * *', 'index.php');

    }

	public function index()
	{
		$this->load->view('index.php');
	}
    public function hello()
    {
        $this->load->view('hello.php');
    }

	
	public function get_history_time() {
		$time = $this->HistoryTime->getHistoryTime();
	   
	    echo json_encode($time);
	    exit();
	}
	
	public function set_history_time() {
		$post_data = $this->input->post('time');
	    $time = $this->HistoryTime->setHistoryTime($post_data);
	   
	    echo json_encode('success');
	    exit();
	}
	
	public function del_history_time() {
		$this->HistoryTime->delHistoryTime();
	}

	public function run_modul(){

        $site_prom_ua = 'NE'; // market_id;
        $prom_ua_key = 'wKr6EtGE6O23vSz3V7A3RjTTOmkDV_H4';    // rozetka api key prom_ua market

        $site_hls = 'HLS'; // market_id;
        $hls_key = '8tYHUX-4PXqZEV1sBT1E6rqTC2u3PDJO'; // rozetka api key of Holysaints market

        $site_tvn = 'EJ'; // market_id;
        $tvn_key = 'Q5Hk2gcHSznl5CvBP1SgMx3-hLBg0xK1'; // rozetka api key of TVN market

        $site_sh = 'NE'; // market_id;
        $sh_key = 'vE8mUxc7RDAHwpdjcCOY1FqlexNFQ5cq'; // rozetka api key of Swiss House market

        $info = array(
            'tvn_key'     => $tvn_key,
            'prom_ua_key' => $prom_ua_key,
            'site_tvn'    => $site_tvn,
            'site_prom_ua' => $site_prom_ua,
            'site_hls'      => $site_hls,
            'hls_key'        => $hls_key,
            'site_sh'      => $site_sh,
            'sh_key'        => $sh_key
        );


        //$this->from_old_crm_to_new_crm();
        $this->from_new_crm_to_rozetka($info);

    }

    /*public function run_modul(){
        $site_prom_ua = 'prom-ua'; // market_id;
        $prom_ua_key = 'NFBz5CTxuh1CnyfCb5I3KpZvtkhdkOYX';    // rozetka api key prom_ua market

        $site_hls = 'HLS'; // market_id;
        $hls_key = 'mpx69AyHQ1l8ulscNL4MVWejlfETuBf0'; // rozetka api key of Holysaints market

        $site_tvn = 'EJ'; // market_id;
        $tvn_key = 'bl1CJckrdN3QsjCFgQ-yKIpgxYbIq2sE'; // rozetka api key of TVN market

        $info = array(
            'tvn_key'     => $tvn_key,
            'prom_ua_key' => $prom_ua_key,
            'site_tvn'    => $site_tvn,
            'site_prom_ua' => $site_prom_ua,
            'site_hls'      => $site_hls,
            'hls_key'        => $hls_key

        );

        $this->from_crm_to_rozetka($info);

        $this->from_rozetka_to_crm($tvn_key, $site_tvn);

        $this->from_rozetka_to_crm($hls_key, $site_hls);

        $this->from_rozetka_to_crm($prom_ua_key, $site_prom_ua);


    }*/


    public function run_tvn(){
        $site_tvn = 'EJ'; // market_id;
        $tvn_key = 'Q5Hk2gcHSznl5CvBP1SgMx3-hLBg0xK1'; // rozetka api key of TVN market

        $this->from_rozetka_to_crm($tvn_key, $site_tvn);
    }

    public function run_prom_ua(){
        $site_prom_ua = 'NE'; // market_id;
        $prom_ua_key = 'wKr6EtGE6O23vSz3V7A3RjTTOmkDV_H4';    // rozetka api key prom_ua market

        $this->from_rozetka_to_crm($prom_ua_key, $site_prom_ua);
    }

    public function run_hls(){
        $site_hls = 'HLS'; // market_id;
        $hls_key = '8tYHUX-4PXqZEV1sBT1E6rqTC2u3PDJO'; // rozetka api key of Holysaints market

        $this->from_rozetka_to_crm($hls_key, $site_hls);
    }

    public function run_sh(){
        $site_sh = 'NE'; // market_id;
        $sh_key = 'vE8mUxc7RDAHwpdjcCOY1FqlexNFQ5cq'; // rozetka api key of Swiss House market

        $this->from_rozetka_to_crm($sh_key, $site_sh);
    }




    public function from_rozetka_to_crm($x, $site) {

        $new_status_orders = $this->order_info_rozetka(1, $x); // order data from rozetka with status 1

        //$client_confirmed_status_orders = $this->order_info_rozetka(2, $x); // order data from rozetka with status 2

        $proc_status_orders = $this->order_info_rozetka(26, $x); // order data from rozetka with status 26

        if (count($new_status_orders['content']['orders']) > 0) {
            $this->send_order_to_crm($new_status_orders['content']['orders'],'new', $site, $x);
        }
        //if (count($client_confirmed_status_orders['content']['orders']) > 0) {
            //$this->send_order_to_crm($client_confirmed_status_orders['content']['orders'],'client-confirmed', $site);
        //}
        if (count($proc_status_orders['content']['orders']) > 0) {
            $this->send_order_to_crm($proc_status_orders['content']['orders'],'proc', $site, $x);
        }

    }

    function send_order_to_crm($order_data, $status, $site, $x){

        $statuses = array(
            'extendedStatus' => array(
                'new',
                'proc',
                'send-to-assembling',
                'availability-confirmed',
                'client-confirmed',
                'no-call',
                'no-product',
                'already-buyed',
                'delyvery-did-not-suit',
                'prices-did-not-suit',
                'cancel-other',
                'offer-analog',
                'op',
                'vvo'
            )
        );

        $shops = $this->get_shop_list();

        foreach ($shops['sites'] as $shop){
            //var_dump($shop);die();
           
            $shop_list[]= $shop['code'];
        }

        $orders_crm = $this->get_order_from_crm_by_status($shop_list);
        //echo '<pre>';
        //var_dump($order_data);die();

        foreach ($order_data as $order) {
            

            if($status == 'proc'){
                if (strpos($order['comment'], 'Дубликат заказ') !== false) {
                    
                    $create_order = true;
                    foreach($orders_crm['orders'] as $order_crm) {
                        if($order_crm['externalId'] == $order['id'] ){
                            $create_order = false;
                            break;
                        }
                    }
                } else {
                    $create_order = false;
                }
            } else {
                $create_order = true;
                foreach($orders_crm['orders'] as $order_crm) {
                    if($order_crm['externalId'] == $order['id'] ){
                        $create_order = false;
                        break;
                    }
                }
            }

            //$payment_status = $this->order_info_rozetka_status_payment($order['id'], $x);


            //if($payment_status['content']['name'] != 'paid' && $order['payment_type_name'] == 'Visa/MasterCard_LiqPay' ){
                //$create_order = false;
            //}


            if($create_order ) {

                $fio = explode(" ", $order['delivery']['recipient_title']);
                $order_first_name = null;
                $order_last_name = null;
                $order_patronymic = null;
                if (!empty($fio[1])) {
                    $order_first_name = $fio[1];
                }
                if (!empty($fio[0])) {
                    $order_last_name = $fio[0];
                }
                if (!empty($fio[2])) {
                    $order_patronymic = $fio[2];
                }
                $order_number = intval($order['id']);
                $order_phone = $order['user_phone'];
                $order_email = substr($order['user']['email'], 0, 49);
                $order_email_array = explode(" ", $order_email);
                foreach ($order_email_array as $email) {
                    if (strpos($email, '@')) {
                        $order_email = $email;
                    }
                }
                if (!filter_var($order_email, FILTER_VALIDATE_EMAIL)) {
                    $order_email = $order_number."_mail@gmail.com";
                }

                if($order['payment_type_name'] == 'Оплата картой Visa/MasterCard (LiqPay)'){

                    $order_payments = array(
                        array(
                            'type' => '20',
                            'status' => 'invoice'
                        )
                    );
                }/*else {
                    $order_payments = array(
                        array(
                            'type' => 'cash',   
                            'status' => 'not-paid'
                        )
                    );
                }*/


                $order_customer_comment = $order['comment'] . '  ' . $order['delivery']['city']['name'] . ((isset($order['delivery']['place_number'])) ? ', Отделение №' : ', ') . $order['delivery']['place_number'] . ', ' . $order['delivery']['place_street'] . ', ' . $order['delivery']['place_house']. ', ' . $order['delivery']['place_flat'];
				$order_customer_comment = $order_customer_comment.(($order['callback_off']==0)? 'Передзвонити':' Покупець впевнений в замовленні, не передзвонювати');
				$a = 0;
                $order_items = array();
                foreach ($order['purchases'] as $item) {
                    if ($a == 0) {
                        $article = $item['item']['article'];
                        $order_items = array(
                            array(
                                'offer' => array(
                                    'externalId' => $item['item']['price_offer_id']
                                ),
                                'quantity' => intval($item['quantity']),
                                'initialPrice' => intval($item['price']),
                                'discountManualAmount' => (intval($item['cost']) - intval($item['cost_with_discount'])) / intval($item['quantity']),
                                'status' => "new"
                            )
                        );
                    }
                    if ($article == $item['item']['article'] && $a != 0) {
                        $item_info = ' Артикул: ' . $item['item']['article'] . ' Ціна: ' . $item['price'] . ' Ціна з знижкою: ' . $item['cost_with_discount'];
                        $order_customer_comment = $order_customer_comment . $item_info;
                    } else {
                        $order_items[$a] = array(
                            'offer' => array(
                                'externalId' => $item['item']['price_offer_id']
                            ),
                            'quantity' => intval($item['quantity']),
                            'initialPrice' => intval($item['price']),
                            'discountManualAmount' => (intval($item['cost']) - intval($item['cost_with_discount'])) / intval($item['quantity']),
                            'status' => "new"
                        );
                    }

                    $a++;
                }
                $order_delivery = null;
                if ($order['delivery']['delivery_service_id'] = "Новая Почта") {
                    $order_delivery = array(
                        'code' => 'np'
                    );
                }
                $order_status = $status;

                switch ( $x ) {
                    case 'wKr6EtGE6O23vSz3V7A3RjTTOmkDV_H4':  // rozetka visim
                         $orderMethod = "rozetka-visim";
                        break;
                    case 'vE8mUxc7RDAHwpdjcCOY1FqlexNFQ5cq':  // rozetka sh
                        $orderMethod = "rozetka-swiss-house";
                        break;
                    
                    default:
                        $orderMethod = "shopping-cart";
                        break;
                }

                if(isset($order_payments)){
                    $order_to_crm = array(
                        'firstName' => $order_first_name,
                        'lastName' => $order_last_name,
                        'patronymic' => $order_patronymic,
                        'number' => $order_number,
                        'phone' => $order_phone,
                        'email' => $order_email,
                        'payments' => $order_payments,
                        'customerComment' => $order_customer_comment,
                        'items' => $order_items,
                        'delivery' => $order_delivery,
                        'status' => $order_status,
                        'externalId' => $order_number,
                        'orderMethod' => $orderMethod
                    );
                } else {
                    $order_to_crm = array(
                        'firstName' => $order_first_name,
                        'lastName' => $order_last_name,
                        'patronymic' => $order_patronymic,
                        'number' => $order_number,
                        'phone' => $order_phone,
                        'email' => $order_email,
                        'customerComment' => $order_customer_comment,
                        'items' => $order_items,
                        'delivery' => $order_delivery,
                        'status' => $order_status,
                        'externalId' => $order_number,
                        'orderMethod' => $orderMethod
                    );

                }

                
                $this->create_order($order_to_crm, $site);

                foreach ($orders_crm['orders'] as $order_crm){
                    if($order_to_crm['externalId'] == $order_crm['externalId']){
                        if($order_crm['summ'] != $order['amount'] ){
                            $this->load->library('email');
                            $config = Array(
                                'protocol' => 'smtp',
                                'smtp_host' => 'ssl://smtp.googlemail.com',
                                'smtp_port' => 465,
                                'smtp_user' => 'helen.visim@gmail.com', // change it to yours
                                'smtp_pass' => 'Helen.Visim.2018', // change it to yours
                                'mailtype' => 'text',
                                'charset' => 'utf-8',
                                'wordwrap' => TRUE
                            );

                            $this->email->initialize($config);

                            $this->email->set_newline("\r\n");
                            $this->email->from('visim@gmail.com', 'Visim Alert'); // change it to yours
                            $this->email->to('helen.visim@gmail.com');// change it to yours
                            $this->email->subject('Замовлення з різними цінами товару');
                            $this->email->message('Ціна не співпадає, замовлення '. $order_crm['externalId']);
                            $this->email->send();
                        }
                    }
                }

            }


        }

    }




    public function get_shop_list() {
        $data  = $this->doCurl("https://wordpress-visim.retailcrm.ru/api/v5/reference/sites?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g", false, 'GET');

        $data = json_decode($data);

        $data = json_decode(json_encode($data), True);

        return $data;
    }

    public function from_new_crm_to_rozetka($info)
    {

        $last_riquest = $this->HistoryTime->getHistoryTime();  // time of the last request

        $history_changes = $this->get_history($last_riquest['history_time']); // get changes from crm from last request

        $created_time_history = $history_changes['generatedAt'];

        $this->HistoryTime->setHistoryTime($created_time_history);

        $this->del_history_time();

        if(count($history_changes['history']) > 0 ) {
            foreach ($history_changes['history'] as $history_change) {     // iterate them
                if (!isset($history_change['created']) && !isset($history_change['deleted'])) {

                    $id = $history_change['order']['id']; //internal id of order in crm
                    $field_status = $history_change['field']; //the field that has changed

                   $this->from_new_crm_to_rozetka_sent($field_status, $history_change, $id, $info);
                }
            }
        }
    }

    public function from_new_crm_to_rozetka_sent($field_status, $history_change, $id, $info){

        $order_crm = $this->get_order_from_crm_by_id($id);
		$external_id = $order_crm['order']['externalId'];
			if($field_status == 'status' && $history_change['newValue']['code'] != 'offer-analog'){     // change status on rozetka

				$status = $this->get_status($history_change['newValue']['code']);

				if(isset($status)) {
					if($order_crm['order']['orderMethod'] == 'rozetka-visim') {
						$response = $this->change_order_on_rozetka($external_id, $info['prom_ua_key'], $status); // post data to rozetka visim
					} else if($order_crm['order']['orderMethod'] == 'rozetka-swiss-house'){
						$this->change_order_on_rozetka($external_id, $info['sh_key'], $status); // post data to rozetka sh
					}
				}

			}
			else if($field_status == 'integration_delivery_data.track_number' /*&& $history_change['order']['site'] == $site*/){   // add delivery track_number

				$track_number = $history_change['newValue'];

				if($order_crm['order']['orderMethod'] == 'rozetka-visim') {
					$this->change_order_on_rozetka($external_id, $info['prom_ua_key'], 3, $track_number); // post data to rozetka
				} else if($order_crm['order']['orderMethod'] == 'rozetka-swiss-house'){
					$this->change_order_on_rozetka($external_id, $info['sh_key'], 3, $track_number); // post data to rozetka
				}

				if(isset($order_crm['order']['customFields']['moyskladexternalid'])){
					$this->moysklad_retail->update_moysklad_ttn($order_crm['order']['customFields']['moyskladexternalid'], $track_number);
				}

			}
			else if($field_status == 'order_product' /*&& $history_change['order']['site'] == $site*/) {  // add product to order
				$item_id = $history_change['item']['id'];

				$order_info = $this->get_order_from_crm_by_id($id);   // get info about order by id

				foreach ($order_info['order']['items'] as $item) {
					if ($item_id == $item['id']) {
						$purchases = array(array(
							'id' => null,
							'cost' => $item['initialPrice'] * $item['quantity'],
							'price' => $item['initialPrice'],
							'price_with_discount' => $item['initialPrice'],
							'quantity' => $item['quantity'],
							'item_id' => $item['offer']['externalId'],
							'item_name' => $item['offer']['name'],
							'kit_id' => null
						));
					}

				}
				$new_item = array(
					'amount' => $order_info['order']['summ'],
					'amount_with_discount' => $order_info['order']['totalSumm'],
					'purchases' => $purchases
				);

				if($order_crm['order']['orderMethod'] == 'rozetka-visim') {
					$this->add_product_to_order($info['prom_ua_key'], $external_id, $new_item);
				} else if($order_crm['order']['orderMethod'] == 'rozetka-swiss-house'){
					$this->add_product_to_order($info['sh_key'], $external_id, $new_item);
				}

			}
			else if($field_status == 'order_product.quantity' /*&& $history_change['order']['site'] == $site*/) {

				$item_id = $history_change['item']['id'];

				$order_info = $this->get_order_from_crm_by_id($id);   // get info about order by id

				$order_info_rozetka = $this->order_info_rozetka_by_id(intval($external_id), $info['prom_ua_key']);
				if($order_info_rozetka ['success'] == false) {
					$order_info_rozetka = $this->order_info_rozetka_by_id(intval($external_id), $info['sh_key']);
				}
				foreach ($order_info['order']['items'] as $item) {

					if ($item_id == $item['id']) {
						$index = array_search($item,$order_info['order']['items']);
						$purchases =  array(array(
							'id' => $order_info_rozetka['content']['purchases'][$index]['id'],
							'quantity' => $item['quantity'],
							'cost' => $item['quantity'] * $order_info['order']['items'][$index]['initialPrice'],
							'cost_with_discount' => $item['quantity'] * ($order_info['order']['items'][$index]['initialPrice'] - $order_info['order']['items'][$index]['discountTotal'])
						));
					}

				}
				$order = array("amount" => $order_info['order']['summ'], "amount_with_discount" => $order_info['order']['totalSumm'], "purchases" => $purchases);

				if($order_crm['order']['orderMethod'] == 'rozetka-visim') {
					$this->add_product_to_order($info['prom_ua_key'], $external_id, $order);
				} else if($order_crm['order']['orderMethod'] == 'rozetka-swiss-house'){
					$this->add_product_to_order($info['sh_key'], $external_id, $order);
				}
			}
			else if($field_status == 'custom_moyskladexternalid') {

				if(isset($order_crm['order']['customFields']['moyskladexternalid'])){

					$this->moysklad_retail->update_moysklad_project($order_crm['order']['customFields']['moyskladexternalid'], $order_crm['order']['orderMethod']);
					$this->moysklad_retail->update_moysklad_payedSum($order_crm['order']['customFields']['moyskladexternalid'], $order_crm['order']['prepaySum']);
				}
			}
    }

    function add_product_to_order($x, $external_id, $new_item){
        $post_arr = $new_item;

        $headers = array("Authorization: Bearer " . $x, "ContentType: application/json");

        $res = $this->doCurl("https://api-seller.rozetka.com.ua/orders/" . $external_id, $headers, 'PUT', $post_arr);
        return $res;
    }

    function order_info_rozetka($status, $x){
        $headers = array("Authorization: Bearer " . $x);

        $orders_info  = $this->doCurl("https://api-seller.rozetka.com.ua/orders/search?expand=user,delivery,purchases,payment_type_name&&status=" . $status, $headers, 'GET');
        $orders_info = json_decode($orders_info);
        $orders_info = json_decode(json_encode($orders_info), True);

        return $orders_info;
    }
    function order_info_rozetka_by_id($id, $x){
        $headers = array("Authorization: Bearer " . $x);

        $orders_info  = $this->doCurl('https://api-seller.rozetka.com.ua/orders/'. $id . '?expand=user,delivery,purchases,payment_type_name', $headers, 'GET');
        $orders_info = json_decode($orders_info);
        $orders_info = json_decode(json_encode($orders_info), True);
       
        return $orders_info;
    }

    function order_info_rozetka_status_payment($id, $x){
        $headers = array("Authorization: Bearer " . $x);

        $orders_info  = $this->doCurl('https://api-seller.rozetka.com.ua/orders/status-payment/'. $id, $headers, 'GET');
        $orders_info = json_decode($orders_info);
        $orders_info = json_decode(json_encode($orders_info), True);

        return $orders_info;
    }

    public function on_rozetka(){
        $data = array(
            'externalId'=> '216538347',
            'key' => 'NFBz5CTxuh1CnyfCb5I3KpZvtkhdkOYX',
            'status' => 3,
            'track_number' => '20400119245812'
        );

        $change_order_on_rozetka  = $this->newCurlRozetka($data);

    }

    public function on_rozetka_new(){
        $data = array(
            'externalId'=> '216630477',
            'key' => 'NFBz5CTxuh1CnyfCb5I3KpZvtkhdkOYX',
            'status' => 2,
        );

        $change_order_on_rozetka  = $this->newCurlRozetka($data);
        //var_dump($change_order_on_rozetka);die();
    }

    function change_order_on_rozetka($externalId, $x, $status, $track_number = null) {
	    
        if($status != 3){
            $headers = array("Authorization: Bearer " . $x);

            /*if(isset($status)) {
                $post_arr = array('status' => $status);
            } elseif (isset($track_number)) {
                $post_arr = array('ttn' => $track_number);
            } else {
                return true;
            }*/

            $post_arr = array('status' => $status);

            $change_order_on_rozetka  = $this->doCurl("https://api-seller.rozetka.com.ua/orders/" . $externalId, $headers, 'PUT', $post_arr);

            $change_order_on_rozetka = json_decode($change_order_on_rozetka);
            $change_order_on_rozetka = json_decode(json_encode($change_order_on_rozetka), True);
            return $change_order_on_rozetka;
        } elseif($status == 3 && isset($track_number)) {
            $data = array(
                'externalId'=> $externalId,
                'key' => $x,
                'status' => $status,
                'track_number' => $track_number
            );

            $change_order_on_rozetka  = $this->newCurlRozetka($data);
			$change_order_on_rozetka = json_decode($change_order_on_rozetka);
			$change_order_on_rozetka = json_decode(json_encode($change_order_on_rozetka), True);
			return $change_order_on_rozetka;

            return $change_order_on_rozetka;
        }
    }

    function get_order_from_crm_by_id($id) {
    
        $data  = $this->doCurl("https://wordpress-visim.retailcrm.ru/api/v5/orders/" . $id ."?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g&&by=id", false, 'GET');

        $data = json_decode($data);
        $data = json_decode(json_encode($data), True);

        return $data;

    }
    function get_order_from_crm_by_external_id($id, $site) {

        $data  = $this->doCurl("https://wordpress-visim.retailcrm.ru/api/v5/orders/" . $id ."?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g&&by=externalId&&site=".$site, false, 'GET');

        $data = json_decode($data);

        $data = json_decode(json_encode($data), True);
        
        return $data;

    }
    function get_order_from_crm_by_status($shops) {
        $url = "https://wordpress-visim.retailcrm.ru/api/v5/orders?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g"
        . "&&filter[extendedStatus][]=new&&filter[extendedStatus][]=proc&&filter[extendedStatus][]=client-confirmed&&filter[extendedStatus][]=redirect&&filter[extendedStatus][]=offer-analog"
        ."&&filter[extendedStatus][]=op&&limit=100";

        foreach ($shops as $key => $value) {
            $url = $url."&&filter[sites][]=".$value;
        }

        $status = array('new', 'redirect', 'client-confirmed');

        $data  = $this->doCurl($url, false, 'GET');

        $data = json_decode($data);

        $data = json_decode(json_encode($data), True);

        return $data;

    }

    function get_status_crm() {

        $extendedStatus = array(
            'new',
            'proc',
            'send-to-assembling',
            'availability-confirmed',
            'client-confirmed',
            'no-call',
            'no-product',
            'already-buyed',
            'delyvery-did-not-suit',
            'prices-did-not-suit',
            'cancel-other',
            'offer-analog',
            'op'
        );

        $obj = (object)$extendedStatus;

        return $obj;
    }

    function get_status($data){
        switch($data) {
            case 'new':
                $status = 1;
                return $status;
                break;
            case 'send-to-assembling':
                $status = 3;
                return $status;
                break;
            case 'client-confirmed':
                $status = 2;
                return $status;
                break;
            case 'no-call':
                $status = 18;
                return $status;
                break;
            case 'no-product':
                $status = 42;
                return $status;
                break;
            case 'already-buyed':
                $status = 41;
                return $status;
                break;
            case 'delyvery-did-not-suit':
                $status = 35;
                return $status;
                break;
            case 'prices-did-not-suit':
                $status = 33;
                return $status;
                break;
            case 'cancel-other':
                $status = 40;
                return $status;
                break;
            case 'proc':
                $status = 26;
                return $status;
                break;
            case 'op':
                $status = 2;
                return $status;
                break;
            case 'offer-analog':
                $status = 26;
                return $status;
                break;
            case 'nvrz':
                $status = 32;
                return $status;
                break;
            case 'nvso':
                $status = 36;
                return $status;
                break;
            case 'omn1':
                $status = 47;
                return $status;
                break;
            case 'omn2':
                $status = 48;
                return $status;
                break;
            case 'mark':
                $status = 6;
                return $status;
                break;
            case 'vidmk':
                $status = 45;
                return $status;
                break;
            case 'nnvn':
                $status = 26;
                return $status;
                break;
            case 'potpidnay':
                $status = 26;
                return $status;
                break;
            case 'availability-confirmed':
                $status = 26;
                return $status;
                break;
            case 'pnpo':
                $status = 26;
                return $status;
                break;
            case 'vtpp':
                $status = 26;
                return $status;
                break;
            case 'srpp':
                $status = 26;
                return $status;
                break;
            case 'chtpp':
                $status = 26;
                return $status;
                break;
            case 'ptpp':
                $status = 26;
                return $status;
                break;
            case 'sbpp':
                $status = 26;
                return $status;
                break;
            case 'ndpp':
                $status = 26;
                return $status;
                break;
            case 'nettn':
                $status = 15;
                return $status;
                break;
            case 'pnpn':
                $status = 2;
                return $status;
                break;
            case 'vtpn':
                $status = 2;
                return $status;
                break;
            case 'srpn':
                $status = 2;
                return $status;
                break;
            case 'chtpn':
                $status = 2;
                return $status;
                break;
            case 'ptpn':
                $status = 2;
                return $status;
                break;
            case 'sbpn':
                $status = 2;
                return $status;
                break;
            case 'ndpn':
                $status = 2;
                return $status;
                break;
            case 'prepayed':
                $status = 2;
                return $status;
                break;
            case 'naypid':
                $status = 26;
                return $status;
                break;
            case 'sppp':
                $status = 26;
                return $status;
                break;
            case 'oppnn':
                $status = 26;
                return $status;
                break;
        }
    }

    function get_history($time_from) {
        $headers = array('Access-Control-Allow-Origin: *','Content-Type: application/json');

        $history_response = $this->doCurl("https://wordpress-visim.retailcrm.ru/api/v5/orders/history?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g&&limit=100&&filter[startDate]=" . $time_from, $headers, 'GET');
        $historys = json_decode($history_response);
        $historys = json_decode(json_encode($historys), True);

        return $historys;
    }

    function get_order_from_crm($statuses = null ) {

        $client = new \RetailCrm\ApiClient(
            'https://wordpress-visim.retailcrm.ru',
            '3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g',
            \RetailCrm\ApiClient::V4
        );

        try {
            $response = $client->request->ordersList($statuses,null, 100);
        } catch (\RetailCrm\Exception\CurlException $e) {
            echo "Connection error: " . $e->getMessage();
        }

        if ($response->isSuccessful()) {
            return $response;

            //echo $response
            // or $response['order']['totalSumm'];
            // or
            //    $order = $response->getOrder();
            //    $order['totalSumm'];
        } else {
            echo sprintf(
                "Error: [HTTP-code %s] %s",
                $response->getStatusCode(),
                $response->getErrorMsg()
            );

            // error details
            if (isset($response['errors'])) {
                print_r($response['errors']);
            }
        }
    }

    function create_order($order_to_crm, $site) {

        $client = new \RetailCrm\ApiClient(
            'https://wordpress-visim.retailcrm.ru',
            '3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g',
            \RetailCrm\ApiClient::V5
        );

        try {

             $response = $client->request->ordersCreate($order_to_crm, $site);


        } catch (\RetailCrm\Exception\CurlException $e) {
            echo "Connection error: " . $e->getMessage();
        }


    }

    function create_order_old_crm($order_to_crm, $site) {

        $client = new \RetailCrm\ApiClient(
            'https://prom2.retailcrm.ru',
            '3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g',
            \RetailCrm\ApiClient::V5
        );

        try {

            $client->request->ordersCreate($order_to_crm, $site);


        } catch (\RetailCrm\Exception\CurlException $e) {
            echo "Connection error: " . $e->getMessage();
        }


    }


    public function doCurl($url, $headers, $method = 'GET', $post_arr = array()) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_USERAGENT      => "test", // name of client
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        if ($method === 'POST') {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_arr);
        } elseif ($method === 'PUT') {
            // Set options necessary for request.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_arr));

        }

        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $res = curl_exec($ch);

        //echo curl_getinfo($ch) . '<br/>';
        //echo curl_errno($ch) . '<br/>';
        //echo curl_error($ch) . '<br/>';



        curl_close($ch);


        return $res;

    }


    public function newCurlRozetka($data) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api-seller.rozetka.com.ua/orders/".$data['externalId'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_POSTFIELDS => "{\n\t\"status\":".$data['status'].",\n\t\"ttn\":\"".$data['track_number']."\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer ".$data['key'],
                "Content-Type: application/json",
                "cache-control: no-cache"
            ),
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);


        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return $response;

        /*if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }*/
    }

    function sent_order_to_old_crm ($order, $status, $site){


        $fio = explode(" ", $order['delivery']['recipient_title']);
        $order_first_name = null;
        $order_last_name = null;
        $order_patronymic = null;
        if (!empty($fio[1])) {
            $order_first_name = $fio[1];
        }
        if (!empty($fio[0])) {
            $order_last_name = $fio[0];
        }
        if (!empty($fio[2])) {
            $order_patronymic = $fio[2];
        }
        $order_number = intval($order['id']);
        $order_phone = $order['user_phone'];
        $order_email = substr($order['user']['email'], 0, 49);
        $order_email_array = explode($order_email, " ");
        foreach ($order_email_array as $email) {
            if (strpos($email, '@')) {
                $order_email = $email;
            }
        }
        if($order['payment_type_name'] == 'Visa/MasterCard_LiqPay'){

            $order_payments = array(
                array(
                    'type' => 'bank-card',
                    'status' => 'paid'
                )
            );
        } else {
            $order_payments = array(
                array(
                    'type' => 'cash',
                    'status' => 'not-paid'
                )
            );
        }

        $order_customer_comment = $order['comment'] . '  ' . $order['delivery']['city']['name'] . ((isset($order['delivery']['place_number'])) ? ', Отделение №' : ', ') . $order['delivery']['place_number'] . ', ' . $order['delivery']['place_street'] . ', ' . $order['delivery']['place_house']. ', ' . $order['delivery']['place_flat'];

        $a = 0;
        $order_items = array();
        foreach ($order['purchases'] as $item) {
            if ($a == 0) {
                $article = $item['item']['article'];
                $order_items = array(
                    array(
                        'offer' => array(
                            'externalId' => $item['item']['article']
                        ),
                        'quantity' => intval($item['quantity']),
                        'initialPrice' => intval($item['price']),
                        'discountManualAmount' => (intval($item['cost']) - intval($item['cost_with_discount'])),
                        'status' => "new"
                    )
                );
            }
            if ($article == $item['item']['article'] && $a != 0) {
                $item_info = ' Артикул: ' . $item['item']['article'] . ' Ціна: ' . $item['price'] . ' Ціна з знижкою: ' . $item['cost_with_discount'];
                $order_customer_comment = $order_customer_comment . $item_info;
            } else {
                $order_items[$a] = array(
                    'offer' => array(
                        'externalId' => $item['item']['article']
                    ),
                    'quantity' => intval($item['quantity']),
                    'initialPrice' => intval($item['price']),
                    'discountManualAmount' => (intval($item['cost']) - intval($item['cost_with_discount'])),
                    'status' => "new"
                );
            }

            $a++;
        }
        $order_delivery = null;
        if ($order['delivery']['delivery_service_id'] = "Новая Почта") {
            $order_delivery = array(
                'code' => 'np'
            );
        }
        $order_status = $status;

        $order_to_crm = array(
            'firstName' => $order_first_name,
            'lastName' => $order_last_name,
            'patronymic' => $order_patronymic,
            'number' => $order_number,
            'phone' => $order_phone,
            'email' => $order_email,
            'payments' => $order_payments,
            'customerComment' => $order_customer_comment,
            'items' => $order_items,
            'delivery' => $order_delivery,
            'status' => $order_status,
            'externalId' => $order_number
        );
        $this->create_order_old_crm($order_to_crm, $site);

    }

    function from_old_crm_to_new_crm(){
        $last_riquest = $this->HistoryTime->getHistoryTimeOldCrm();  // time of the last request

        $history_changes = $this->getHistoryOldCrm($last_riquest['time']); // get changes from crm from last request

        $created_time_history = $history_changes['generatedAt'];
        $this->HistoryTime->delHistoryTimeOldCrm();
        $this->HistoryTime->setHistoryTimeOldCrm($created_time_history);


        if(count($history_changes['history']) > 0 ) {
            foreach ($history_changes['history'] as $history_change) {     // iterate them

                if (!isset($history_change['created']) && !isset($history_change['deleted'])) {
                    $field_status = $history_change['field']; //the field that has changed
                    $site = $history_change['order']['site'];

                    if($field_status == 'status' && $history_change['newValue']['code'] != 'offer-analog' /*&& $history_change['order']['site'] == $site*/) {     // change status on rozetka

                        $status = $this->get_status($history_change['newValue']['code']);

                        if (isset($status)) {
                            $order_to_crm = array(
                                'order' => json_encode (array(
                                    'externalId'    => $history_change['order']['externalId'],
                                    'status'        => $history_change['newValue']['code']
                                )),
                                'by' => 'externalId',
                                'site' => $site

                            );

                            $url = "https://wordpress-visim.retailcrm.ru/api/v5/orders/". $history_change['order']['externalId'] ."/edit?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g";
                            $response = $this->doCurl($url, false, $method = 'POST', $order_to_crm);

                        }
                    }
                    else if($field_status == 'integration_delivery_data.track_number' /*&& $history_change['order']['site'] == $site*/){   // add delivery track_number
                        $track_number = $history_change['newValue'];

                        $delivery  = array(
                            'data' => array('trackNumber' => $track_number)
                        );
                        $order_to_crm = array(
                            'order' => json_encode (array(
                                'externalId'    => $history_change['order']['externalId'],
                                'delivery'        => $delivery
                            )),
                            'by' => 'externalId',
                            'site' => $site
                        );

                        $url = "https://wordpress-visim.retailcrm.ru/api/v5/orders/". $history_change['order']['externalId'] ."/edit?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g";
                        $response = $this->doCurl($url, false, $method = 'POST', $order_to_crm);
                    }

                }
            }
        }
    }

    function getHistoryOldCrm($time_from){

        $headers = array('Access-Control-Allow-Origin: *','Content-Type: application/json');

        $history_response = $this->doCurl("https://prom2.retailcrm.ru/api/v5/orders/history?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g&&limit=100&&filter[startDate]=" . $time_from, $headers, 'GET');
        $historys = json_decode($history_response);
        $historys = json_decode(json_encode($historys), True);

        return $historys;

    }

    public function rozetka_login() {
        $post_arr = array(
            'username' => 'maria.visim',
            'password' => 'c21lcmVjaGFuc2thMTk5NQ=='
        );


        $login_rozetka  = $this->doCurl("https://api-seller.rozetka.com.ua/sites", '','POST', $post_arr);

        var_dump($login_rozetka);die();

    }


    public function get_status_list() {
        $data  = $this->doCurl("https://wordpress-visim.retailcrm.ru/api/v5/reference/statuses?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g", false, 'GET');
        echo '<pre>'; var_dump($data);die();
        return $data;
    }

    /*public function sendReminderEquipment(){

        $reminder_to_time = strtotime(date('Y-m-d H:i:s'));
        $new_time = date("Y-m-d H:i:s", strtotime('-4320 minutes', $reminder_to_time));

        $statuses = array(
            'extendedStatus' => array(
                'vs',
                'up4',
                'up5',
                'vupp',
                's',
                'po',
                'send-to-assembling',
                'upl',
                'assembling-complete',
                'vuch',
                'up2',
                'up3',
                'vup',
                'vpdv',
                'assembling',
                'vus',
                'vuv',
                'do'
            ),
            'statusUpdatedAtTo' => $new_time
        );
        $orders_id = '';
        $orders = $this->get_order_from_crm($statuses);
        foreach ($orders->orders as $order){
            $orders_id .= 'Id: '. strval($order['externalId']).' ';
        }

        // Create the Transport
        try {
            $transport = (new Swift_SmtpTransport('mail.visim.biz', 26))
                ->setUsername('visimteam@visim.biz')
                ->setPassword('h9Bv30myZsqSOTV4');
        } catch (\Swift_TransportException $e) {
            echo $e->getMessage();
        }

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);

        // Create a message
        $message = (new Swift_Message('Замовлення які знаходяться в групі статусів "Комплектація" більше 72 годин '))
            ->setFrom('helen.visim@gmail.com')
            ->setTo('dmytro.ruzak@gmail.com')
            ->setBody('Замовлення які знаходяться в групі статусів "Комплектація" більше 72 годин ' . $orders_id);

        // Send the message
        try {
            $mailer->send($message);
        } catch (\Swift_TransportException $e) {
            echo $e->getMessage();
        }

    }

    public function sendReminderDelivery(){

        $reminder_to_time = strtotime(date('Y-m-d H:i:s'));
        $new_time = date("Y-m-d H:i:s", strtotime('-4320 minutes', $reminder_to_time));

        $statuses = array(
            'extendedStatus' => array(
                'send-to-delivery',
                'delivering',
                'psd',
                'redirect'
            ),
            'statusUpdatedAtTo' => $new_time
        );
        $orders_id = '';
        $orders = $this->get_order_from_crm($statuses);
        foreach ($orders->orders as $order){
            $orders_id .= 'Id: '. strval($order['externalId']).' ';
        }

        // Create the Transport
        $transport = (new Swift_SmtpTransport('mail.visim.biz', 26))
            ->setUsername('visimteam@visim.biz')
            ->setPassword('h9Bv30myZsqSOTV4');

        // Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);

        // Create a message
        $message = (new Swift_Message('Замовлення які знаходяться в групі статусів "Прибуло у відділення. Запрошуємо отримати" більше 72 годин '))
            ->setFrom('helen.visim@gmail.com')
            ->setTo('dmytro.ruzak@gmail.com')
            ->setBody('Замовлення які знаходяться в групі статусів "Прибуло у відділення. Запрошуємо отримати" більше 72 годин ' . $orders_id);

        // Send the message
        try {
            $mailer->send($message);
        } catch (\Swift_TransportException $e) {
            echo $e->getMessage();
        }

    }*/


	function get_all_order_from_crm($sites) {
		$data  = $this->doCurl("https://wordpress-visim.retailcrm.ru/api/v5/orders?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g&&limit=100&&filter[sites][]=".$sites, false, 'GET');

		$data = json_decode($data);

		$data = json_decode(json_encode($data), True);

		$totalCount = $data['pagination']['totalPageCount'];
		$dataAll = array();
		for($i=1; $i <= $totalCount; $i++) {
			$dataInfo  = $this->doCurl("https://wordpress-visim.retailcrm.ru/api/v5/orders?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g&&limit=100&&page=".$i."&&filter[sites][]=".$sites, false, 'GET');
			$dataInfo = json_decode($dataInfo);
			$dataInfo = json_decode(json_encode($dataInfo), True);
			$dataAll = array_merge($dataAll,$dataInfo['orders']);
		}

		return $dataAll;

	}

	public function updateSiteINOrder(){
		$sites = 'wordpress-visim-retailcrm-ru-admin-sites';
		$orders = $this->get_all_order_from_crm($sites);
		var_dump(count($orders));
		foreach ($orders as $order) {
			//echo '<pre>';var_dump($order);
			$order_to_crm = array(
				'order' => json_encode (array(
					'externalId'    => $order['externalId'],
					'orderType'     => $order['orderType']
				)),
				'by' => 'externalId',
				'site' => 'NE'
			);
			//var_dump($order['externalId']);die();
			$url = "https://wordpress-visim.retailcrm.ru/api/v5/orders/". $order['externalId'] ."/edit?apiKey=3sLABNZ1DvceNXI3BdKL8BlyxA9iJQ5g";
			$response = $this->doCurl($url, false, $method = 'POST', $order_to_crm);
			var_dump($response);die();
		}
	}
}
